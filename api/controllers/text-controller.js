const db = require('../config/db-config')
const text = require('../models/text-model')

class textController{

    constructor(){
        this.model = new text();
    }

/**
 * GET Text by id 
 */
    findOne_txt_callback(req,res){
        db.query(this.model.findOne_text(req.params.id),(err,result)=>{
            res.send(this.model.findOne_text_callback(err,result))
        })
    }
/**
 * POST Text 
 */
    create_txt_callback(req,res){
         let data= [[
            req.body.data,
            req.body.image,
            req.body.back
          ]];
             
        db.query(this.model.create_text(),[data],(err,re)=>{
            res.send(this.model.create_text_callback(err,re))
        })
    }
}
module.exports = new textController();