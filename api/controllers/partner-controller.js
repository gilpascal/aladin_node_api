var db = require('../config/db-config');
var partner = require('../models/partner-model');
var sendmail = require('../config/mail/notify')

class partnerController{

    constructor(){
        this.model = new partner();
    }
/**
 * Partner details
 */
    partner_detail_callback(req,res){
        db.query(this.model.partner_details(),(err,result)=>{
            res.send(this.model.partner_details_callback(err,result))
        })
    }

/**
* Validate account
*/
    partner_valid_acc_callback(req,res){
        db.query(this.model.partner_valid_account(),(err,result)=>{
            res.send(this.model.partner_valid_account_callback(err,result))
        })
    }

/**
 * PUT activated
 */
    update_activated_callback(req,res){
        let set_activated = req.body.activated
        db.query(this.model.update_activate(set_activated,req.params.id),(err,result)=>{
            if(err) throw err

            let newsql=`SELECT email FROM aladin_users WHERE user_id=${req.params.id}`;

            db.query(newsql,(err,r)=>{
                if(err) throw err;

                console.log(r)
                let message={
                    to:r[0].email.toLowerCase(),
                    subject:"Aladin password",
                    html:"Votre compte à été activé vous pouvez dersomaire ajouté vos produits."
                };
                
                
                

                res.send({
                    
                    success:true,
                    message:`vous avez approuvé le compte numero: ${req.params.id} et un email à été envoyé ${r[0].email}`
                    
                })
              sendmail(message.to,message.subject,message.html);
              
            });
            //res.send(this.model.update_activate_callback(err,result,));
            
        })
    }

}

module.exports = new partnerController();