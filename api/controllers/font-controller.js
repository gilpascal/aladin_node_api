const db = require('../config/db-config')
const font = require('../models/font-model')
const order_date = require('../config/order-date')

class fontController{
    
    constructor(){
        this.model= new font();
    }

/** Tout testé Ok */
/**
 * POST font
 */
    create_fnt_callback(req,res){
        let date = order_date()
        var value=[[req.body.url,req.body.name,date]]
            
        db.query(this.model.create_font(),[value],(err,result)=>{
            res.send(this.model.create_font_callback(err,result))
        })
    }

/**
 * GET font
 */
    findAll_fnt_callback(req,res){
            
        db.query(this.model.findAll_font(),(err,result)=>{
            res.send(this.model.findAll_font_callback(err,result))
        })
    }

/**
 * GET font by id
 */
    findOne_fnt_callback(req,res){
                
        db.query(this.model.findOne_font(req.params.id),(err,result)=>{
            res.send(this.model.findOne_font_callback(err,result))
        })
    }
}

module.exports = new fontController();