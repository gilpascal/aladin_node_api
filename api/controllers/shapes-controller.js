var db = require('../config/db-config');
var shape = require('../models/shapes-model');
const order_date = require('../config/order-date');

class shapesController{

    constructor(){
        this.model = new shape();
    }
/**
* GET All shapes
*/
    findAll_shapes_callback(req,res){
        const limit=16
        const page= req.query.page
        const offset=(page-1)*limit

        db.query(this.model.findAll_shp(limit,offset),(err,result)=>{
            res.send(this.model.findAll_shp_callback(err,result))
        });
    }

/**
* GET One Shape
*/
    findOne_shapes_callback(req,res){

        db.query(this.model.findOne_shp(req.params.id),(err,result)=>{
            res.send(this.model.findOne_shp_callback(err,result));
        });
    }

/*
* POST shape
*/
    create_shapes_callback(req,res){
        const file = req.files.url
        const date = order_date()
        file.mv("public/images/"+file.name,(err,r)=>{
            if(err){
             throw err;
            }else{
                    var value=[["https://api.aladin.ci/images/"+file.name,req.body.name,date]];
                 
                    db.query(this.model.create_shp(),[value],(err,result)=>{
                        res.send(this.model.create_shp_callback(err,result))
                     
                });
            }
         });
    }
}
module.exports= new shapesController();