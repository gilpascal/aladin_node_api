const db = require('../config/db-config')
const prod_orders = require('../models/prod_order-model')
const order_date = require('../config/order-date')
var sendmail = require('../config/mail/notify')
var ordermail = require('../config/order-mail')

class prod_ordersController{

    constructor(){
        this.model = new prod_orders();
    }


/**
* POST product order
*/
    create_prod_order_callback(req,res){
        let tab = JSON.parse(req.body.items)
        let value=[]
        for(let item of tab){
            value.push([JSON.stringify(item),item.id,req.body.order])
        }

        if(value.length>0){
        
            db.query(this.model.create_p_order,[value],(err,resp)=>{
                if(err) throw err
                let emaildata={
                    name:req.body.name
                }
                let message={
                    to:req.body.email,
                    subject:"Aladin",
                    html:ordermail(emaildata)
                };
                sendmail(message.to,message.subject,message.html);
                res.send(this.model.create_p_order_callback(err,resp))
            })
        }
    }
}

module.exports = new prod_ordersController();
