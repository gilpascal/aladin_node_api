var db = require('../config/db-config');
const users = require('../models/users-model');
const bcrypt=require('bcrypt');
const createuserhtml=require('../config/userhtml');
var sendmail=require('../config/mail/notify');
const order_date=require('../config/order-date');
const pwdhtml = require('../config/pwd');
const createToke = require('../config/token');


class usersController {
    constructor(){
        this.model = new users();
    }

/**
* SIGN UP user
*/
    create_acc_user_callback(req,res){
            
            let password=req.body.pwd
            let date = order_date()
            const saltRounds = 10;
            bcrypt.genSalt(saltRounds, (err, salt)=> {
                bcrypt.hash(password,salt, (err, hash) =>{
                    
                    var data=[[req.body.name,req.body.email.toLowerCase(),hash,req.body.contact,date,req.body.city,req.body.is_partner,req.body.whatsapp,"CI"]];
                    console.log(data)
                    db.query(this.model.create_account_user(),[data],(err,result,token)=>{


                        res.send(this.model.create_account_user_callback(err,createToke({email:req.body.email,username:req.body.name})))

                        let emaildata={
                            name:req.body.name
                        }
            
                        let message={
                            to:req.body.email.toLowerCase(),
                            subject:"Création de compte Aladin",
                            html:createuserhtml(emaildata)
                        };
                        sendmail(message.to,message.subject,message.html);
                        

                        


                    })
                   
                })
                
            })
           
    }

/**
* LOGIN user
*/

    login_usr_callback(req,res){
        let email = req.body.email;
        let password=req.body.pwd;

        db.query(this.model.login_user(),['%' + email + '%'],(err,resultat,field)=>{
            console.log(email);
            if(err) {
                return{
                    status:false,
                    error:err
                }
            }
            
            if(resultat.length>0){
                bcrypt.compare(password,resultat[0].password).then( (result)=> {
                    console.log(password);
                    console.log(resultat[0].password);
                    console.log(result);
                    console.log(resultat.length);
                    res.send(this.model.login_user_callback(err,resultat,result,createToke({email:req.body.email,username:resultat[0].name})));
                    
                    
                })
                 
            }else{
                res.send({
                    status:false,
                    message:"Authentification echoué email introuvable",
                    data:resultat
                })
            }
            
        });
       
    }

/**
* GET All user
*/

   findAll_usr_callback(req,res){
       db.query(this.model.findAll_user(),(err,result)=>{
           res.send(this.model.findAll_user_callback(err,result));
       });
    }

/**
 * Find One user
 */

    findOne_usr_callback(req,res){
        db.query(this.model.findOne_user(req.params.id),(err,result)=>{
            res.send(this.model.findOne_user_callback(err,result));
        });
    }

/**
* Delete One user
*/

    delete_usr_callback(req,res){
        db.query(this.model.delete_user(req.params.id),(err,result)=>{
            res.send(this.model.delete_user_callback(err,result));
        });
    }

/**
* RESET user pwd
*/
    reset_pwd_callback(req,res){
        let password = req.body.pwd; 
        const saltRounds= 10;
        bcrypt.genSalt(saltRounds,(err,salt)=>{
            bcrypt.hash(password,salt,(err,hash)=>{

                db.query(this.model.reset_password(JSON.stringify(hash),req.params.id),(err,result)=>{

                    res.send(this.model.reset_password_callback(err,result));

                    let message={
                        to:req.body.email,
                        subject:"Aladin password",
                        html: pwdhtml({name:"Cher client"})
                    };
                    sendmail(message.to,message.subject,message.html);
                
                });
            })
       })      
    }

/**
* Change Pwd
*/
    change_pwd_callback(req,res){
        const saltRounds=10;
        
        db.query(this.model.change_password(req.params.id),(err,result)=>{
            if(err){
                return{
                    status:false,
                    error:err
                }
            }
            let password = result[0].password;
            console.log(password)
            
            bcrypt.compare(req.body.pwd,password).then((resul)=>{
                if(resul){
                    bcrypt.genSalt(saltRounds, (err, salt)=> {
                        bcrypt.hash(req.body.newpassword,salt, (err, hash)=> {
                            let query=`UPDATE aladin_users SET password=${JSON.stringify(hash)} WHERE user_id=${req.params.id}`;
                            console.log(hash)
                            db.query(query,(err,resultat)=>{
                                res.send(this.model.change_password_callback(err,resul));
         
                            })
                            
                        })
                    })
                    
                }else{
                    res.send({
                        status:false,
                        message:"ancien mot de passe introuvable"
                    })
                }
                
            })

        })
    }


/**
* Update details user
*/
    update_dtls_callback(req,res){
        /*let id = req.params.id;
        let nom = req.body.nom;
        let address = req.body.address;
        let phone = req.body.phone;*/
        
        db.query(this.model.update_details(req.body.nom,req.body.address,req.body.phone,req.params.id),(err,resul)=>{
            res.send(this.model.update_details_callback(err,resul))
        })
    }

/**
* Cheking mail 
*/

    check_mail_callback(req,res){
        let email = req.body.email;

        db.query(this.model.checking_mail(),['%'+email+'%'],(err,result)=>{
            res.send(this.model.checking_mail_callback(err,result))
         
        });
    }
    
}


module.exports = new usersController();