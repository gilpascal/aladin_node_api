var db = require('../config/db-config');
var top = require('../models/tops-model');

class topsController{
    
    constructor(){
        this.model = new top();
    }
/**
* GET All tops
*/
    findAll_tps_callback(req,res){
        db.query(this.model.findAll_tops(),(err,result)=>{
            res.send(this.model.findAll_tops_callback(err,result))
        });
    }
/**
* GET One top
*/
    findOne_tps_callback(req,res){
        db.query(this.model.findOne_tops(req.params.id),(err,result)=>{
            res.send(this.model.findOne_tops_callback(err,result))
        });
    }
/**
* POST top
*/

    create_tps_callback(req,res){
        const file = req.files.img;
        file.mv("public/tops/"+file.name,(err)=>{
            if(err) throw err
            var user=parseInt(req.body.user_id);
            var value=[["https://api.aladin.ci/tops/"+file.name,req.body.price,user,req.body.comment]];
           
            db.query(this.model.create_tops(),[value],(err,result)=>{
                res.send(this.model.create_tops_callback(err,result))
            });
        })
    }

}

module.exports= new topsController();