const db = require('../config/db-config')
const edit_color = require('../models/edit-color-model')

class edit_colorController{
    
    constructor(){
        this.model= new edit_color();
    }

/**
 * GET Color
 */
    findAll_edit_colors_calback(req,res){
        db.query(this.model.findAll_color(),(err,result)=>{
            res.send(this.model.findAll_color_callback(err,result));
        })
    }

/**
 * POST Color
 */
    create_edit_colors_calback(req,res){
        let data =[[req.body.lib,req.body.staff]]
        db.query(this.model.create_edit_color(),[data],(err,result)=>{
            res.send(this.model.create_edit_color_callback(err,result));
        })
    }
}
module.exports = new edit_colorController();