var db = require('../config/db-config');
const staff = require('../models/staff-model');
const bcrypt=require('bcrypt');
const createuserhtml=require('../config/userhtml');
var sendmail=require('../config/mail/notify');
const order_date=require('../config/order-date');
const generator = require('generate-password');
const createToke = require('../config/token')
const pwdhtml = require('../config/pwd')

class staffController {
    constructor(){
        this.model = new staff();
    }
/**
* SIGN UP staff 
*/
    create_acc_staff_callback(req,res){
        
        var password = generator.generate({
            length:10,
            numbers: true
        });
        let date = order_date();
        const saltRounds = 10;
        bcrypt.genSalt(saltRounds, (err, salt) => {
            bcrypt.hash(password,salt, (err, hash) => {

                var data=[[req.body.name,req.body.role,req.body.email,hash,req.body.phone,date]];
                console.log(req.body.email)
                
                db.query(this.model.create_account_staff(),[data],(err,result)=>{
                    
                    res.send(this.model.create_account_staff_callback(err,result,createToke({email:req.body.email.toLowerCase(),username:req.body.name})));
                    let emaildata={
                        password:password,
                        role:req.body.role,
                        name:req.body.name
                    
                    }
            
                    let message={
                        to:req.body.email,
                        subject:"Creation de compte Aladin",
                        html:createuserhtml(emaildata)
                    };
                    sendmail(message.to,message.subject,message.html); 
                   

                    
                });

            })

        })
    }

/**
* LOGIN Staff A revoir
*/
    login_staf_callback(req,res){
        let email = req.body.email;
        let password=req.body.pwd;

        db.query(this.model.login_staff(),['%' + email + '%'],(err,resultat,field)=>{
            console.log(email);
            if(err) {
                res.send({
                    status:false,
                    error:err
                })
            }
            
            if(resultat.length>0){
                bcrypt.compare(password,resultat[0].password).then( (result)=> {
                    console.log(password);
                    console.log(resultat[0].password);
                    console.log(result);
                    console.log(resultat.length);
                    res.send(this.model.login_staff_callback(err,resultat,result,createToke({email:req.body.email,username:resultat[0].name})));
                    
                    
                })
                 
            }/*else{
                res.send({
                    status:false,
                    message:"Authentification echoué email introuvable",
                    data:resultat
                })
            }*/
            
        });
       
    }


/**
* Find All staff 
*/
    findAll_staf_callback(req,res){
        db.query(this.model.findAll_staff(),(err,resp)=>{
            res.send(this.model.findAll_staff_callback(err,resp));
        });
    }

/**
* Find designer from staff 
*/
    findAll_staf_designer_callback(req,res){
        db.query(this.model.findAll_staff_designer(),(err,result)=>{
            res.send(this.model.findAll_staff_designer_callback(err,result));
        });
    }

/**
* Change Staff Password
*/
    change_password_callback(req,res){
        let password =req.body.pwd;
        console.log(req.body.pwd); 
        const saltRounds=10;
        bcrypt.genSalt(saltRounds,(err,salt)=>{
            bcrypt.hash(password,salt,(err,hash)=>{
    
                db.query(this.model.change_pwd(JSON.stringify(hash),req.params.id),(err,result)=>{
                    res.send(this.model.change_pwd_callback(err,result));

                    let message={
                        to:req.body.email,
                        subject:"Aladin password",
                        html: pwdhtml({name:"Cher client"})
                    };
                    sendmail(message.to,message.subject,message.html);
                });
            })
        })
    }

/**
* Reset Password
*/
    reset_pwd_staff_callback(req,res){
        let password = req.body.pwd; 
        const saltRounds= 10;
        bcrypt.genSalt(saltRounds,(err,salt)=>{
            bcrypt.hash(password,salt,(err,hash)=>{

                db.query(this.model.reset_password_staff(JSON.stringify(hash),req.params.id),(err,result)=>{

                    res.send(this.model.reset_password_staff_callback(err,result));

                    let message={
                        to:req.body.email,
                        subject:"Aladin password",
                        html:"Votre mot de passe a été reinitialisé."
                    };
                    sendmail(message.to,message.subject,message.html);
                
                });
            })
       })
    }


/**
* Find conseiller client from staff 
*/
    findAll_staf_cc_callback(req,res){
        db.query(this.model.findAll_staff_cc(),(err,result)=>{
            res.send(this.model.findAll_staff_cc_callback(err,result));
        });
    }

/**
* Find admin from staff 
*/
    findAll_staf_admin_callback(req,res){
        db.query(this.model.findAll_staff_admin(),(err,result)=>{
            res.send(this.model.findAll_staff_admin_callback(err,result));
        });
    }

/**
* Delete One user
*/

    delete_staf_callback(req,res){
        db.query(this.model.delete_staff(req.params.id),(err,result)=>{
            res.send(this.model.delete_staff_callback(err,result));
        });
    }
  
}

module.exports= new staffController();