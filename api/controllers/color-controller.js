const db = require('../config/db-config')
const color = require('../models/color-model')
const order_date = require('../config/order-date')

class colorController{
    
    constructor(){
        this.model= new color();
    }

    create_color_cloth_callback(req,res){
        let date= order_date()
        let data = [["https://api.aladin.ci/cloths/"+req.files.img.name,"https://api.aladin.ci/cloths/"+req.files.link.name,req.body.lib,req.body.insertedId,date]];
        req.files.img.mv("public/cloths/"+req.files.img.name,(err,rest)=>{
            if(err) throw err;
            req.files.link.mv("public/cloths/"+req.files.link.name,(er,result)=>{
                if(er) throw er
                    
                db.query(this.model.create_color_clt(),[data],(error,re)=>{
                    res.send(this.model.create_color_clt_callback(error,re))
                })
                
            })
        })
    }

    create_color_display_callback(req,res){
        let date= order_date()
        let data = [["https://api.aladin.ci/disps/"+req.files.img.name,"https://api.aladin.ci/disps/"+req.files.link.name,req.body.lib,req.body.insertedId,date]];
        req.files.img.mv("public/disps/"+req.files.img.name,(err,rest)=>{
            if(err) throw err;
            req.files.link.mv("public/disps/"+req.files.link.name,(er,result)=>{
                if(er) throw er
                    
                db.query(this.model.create_color_disp(),[data],(error,re)=>{
                    res.send(this.model.create_color_disp_callback(error,re))
                })
                
            })
        })
    }

    create_color_gadget_callback(req,res){
        let date= order_date()
        let data = [["https://api.aladin.ci/gadgets/"+req.files.img.name,"https://api.aladin.ci/gadgets/"+req.files.link.name,req.body.lib,req.body.insertedId,date]];
        req.files.img.mv("public/gadgets/"+req.files.img.name,(err,rest)=>{
            if(err) throw err;
            req.files.link.mv("public/gadgets/"+req.files.link.name,(er,result)=>{
                if(er) throw er
                    
                db.query(this.model.create_color_gadg(),[data],(error,re)=>{
                    res.send(this.model.create_color_gadg_callback(error,re))
                })
                
            })
        })
    }

    create_color_packaging_callback(req,res){
        let date= order_date()
        let data = [["https://api.aladin.ci/packs/"+req.files.img.name,"https://api.aladin.ci/packs/"+req.files.link.name,req.body.lib,req.body.insertedId,date]];
        req.files.img.mv("public/packs/"+req.files.img.name,(err,rest)=>{
            if(err) throw err;
            req.files.link.mv("public/packs/"+req.files.link.name,(er,result)=>{
                if(er) throw er
                    
                db.query(this.model.create_color_pack(),[data],(error,re)=>{
                    res.send(this.model.create_color_pack_callback(error,re))
                })
                
            })
        })
    }

    create_color_printed_callback(req,res){
        let date= order_date()
        let data = [["https://api.aladin.ci/prints/"+req.files.img.name,"https://api.aladin.ci/prints/"+req.files.link.name,req.body.lib,req.body.insertedId,date]];
        req.files.img.mv("public/prints/"+req.files.img.name,(err,rest)=>{
            if(err) throw err;
            req.files.link.mv("public/prints/"+req.files.link.name,(er,result)=>{
                if(er) throw er
                    
                db.query(this.model.create_color_print(),[data],(error,re)=>{
                    res.send(this.model.create_color_print_callback(error,re))
                })
                
            })
        })
    }
}
module.exports = new colorController();