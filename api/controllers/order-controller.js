const db = require('../config/db-config')
const orders = require('../models/order-model')
const order_date = require('../config/order-date')
var sendmail = require('../config/mail/notify')
var ordermail = require('../config/order-mail')


class ordersController{

    constructor(){
        this.model = new orders();
    }
    
/**
* POST order
*/
    create_ord_callback(req,res){
        var user=parseInt(req.body.customer);
        var created=order_date();

        var value=[[req.body.status,user,created,req.body.dmode,req.body.d_place,req.body.total,req.body.city]];
        
        db.query(this.model.create_order,[value],(err,result)=>{
            res.send(this.model.create_order_callback(err,result))
        })
    }

/**
* POST order
*/
create_ord_callback(req,res){
    var user=parseInt(req.body.customer);
    var created=order_date();

    var value=[[req.body.status,user,created,req.body.dmode,req.body.d_place,req.body.total,req.body.city]];
    
    db.query(this.model.create_order,[value],(err,result)=>{
        res.send(this.model.create_order_callback(err,result))
    })
}




/**
* GET All pending order
*/
    findAll_pend_callback(req,res){
        db.query(this.model.findAll_pending(),['%'+ "pending" + '%'],(err,result)=>{
            res.send(this.model.findAll_pending_callback(err,result))
        })
    }

/**
* GET All pending order
*/
    findAll_order_cust_callback(req,res){
        let user= req.params.id;
        db.query(this.model.findAll_order_customer(user),(err,result)=>{
            res.send(this.model.findAll_order_customer_callback(err,result))
        })
    }

/**
* GET All delivered order
*/
    findAll_deliver_callback(req,res){
        db.query(this.model.findAll_delivered(),['%'+"ok"+'%'],(err,r)=>{
            res.send(this.model.findAll_delivered_callback(err,r))
        })
    }

/**
* GET All  new order
*/
    findAll_new_callback(req,res){
        db.query(this.model.findAll_news(),['%'+"new"+'%'],(err,result)=>{
            res.send(this.model.findAll_news_callback(err,result))
        })
    }


/**
* GET All  order rejected
*/
    findAll_reject_callback(req,res){
        db.query(this.model.findAll_rejected(),['%'+"rejected"+'%'],(err,result)=>{
            res.send(this.model.findAll_rejected_callback(err,result))
        })
    }

/**
* UPDATE order status Ok
*/
    update_stts_Ok_callback(req,res){
        let data =[[req.body.status]]

        db.query(this.model.update_status_Ok(req.params.id),[data],(err,result)=>{
            res.send(this.model.update_status_Ok_callback(err,result))
        })
    }

/**
* UPDATE order status pending
*/
    update_stts_pending_callback(req,res){
        let data =[[req.body.status]]

        db.query(this.model.update_status_pending(req.params.id),[data],(err,result)=>{
            res.send(this.model.update_status_pending_callback(err,result))
        })
    }

/**
* UPDATE order status rejected 
*/
update_stts_rejected_callback(req,res){
    let data =[[req.body.status]]

    db.query(this.model.update_status_rejected(req.params.id),[data],(err,result)=>{
        res.send(this.model.update_status_rejected_callback(err,result))
    })
}
 


    


    
}
module.exports = new ordersController();