const db = require('../config/db-config') 
const cliparts = require('../models/cliparts-model')
const order_date = require('../config/order-date')

class clipartsController{
    constructor(){
        this.model = new cliparts();
    }

/**
 * POST cliparts
 */
    create_clips_callback(req,res){

        const file = req.files.url
        const date = order_date()

        file.mv("public/images/"+file.name,(err,r)=>{
            if(err) throw err
            
            var value=[["https://api.aladin.ci/images/"+file.name,req.body.name,date]];
        
            db.query(this.model.create_clipart(),[value],(err,result)=>{
                res.send(this.model.create_clipart_callback(err,result))
            })
       
        });
       
    }

/**
 * GET All cliparts
 */
    findAll_clips_callback(req,res){
        const limit=16
        const page= req.query.page
        const offset=(page-1)*limit
        
        db.query(this.model.findAll_cliparts(limit,offset),(err,result)=>{
            res.send(this.model.findAll_cliparts_callback(err,result))
        })
    }

/**
 * GET One clipart
 */
    findOne_clips_callback(req,res){
        db.query(this.model.findOne_clipart(req.params.id),(err,result)=>{
            res.send(this.model.findOne_clipart_callback(err,result))
        })
    }

}
module.exports = new clipartsController();