var db = require('../../config/db-config');
var packagings = require('../../models/products/packagings-model');
var order_date=require('../../config/order-date');

class packagingscontroller {
    constructor(){
        this.model = new packagings();
    }

    findAll_pack_callback(req,res){
        const limit=6
        const page= req.query.page
        const offset=(page-1)*limit

        db.query(this.model.findAll_pack(limit,offset),(err,result)=>{
            res.send(this.model.findAll_pack_callback(err,result,page));
        })
    }
    findOne_pack_callback(req,res){

        db.query(this.model.findOne_pack(req.params.id),(err,result)=>{
            res.send(this.model.findOne_pack_callback(err,result));
        });
    }

    create_pack_callback(req,res){
        const file = req.files.image;
        let date = order_date();
        file.mv('public/packs/'+file.name,(err,result)=>{
            if(err) throw err

            var data=[[req.body.name,req.body.price,req.body.ordered,date,"https://api.aladin.ci/packs/"+file.name,req.body.staff,req.body.comment,req.body.dim,req.body.material]]
            db.query(this.model.create_pack(),[data],(err,result)=>{

                res.send(this.model.create_packagings_callback(err,result));
            });
        })
    }

/**
*  Update packaging products
*/
    update_pack_callback(req,res){
        db.query(this.model.update_packaging(req.body.ordered,req.params.id),(err,result)=>{

            if(err) throw err;
            let newsql=`SELECT user_id FROM aladin_packaging WHERE pack_id=${req.params.id}`;
            
            db.query(newsql,(err,re)=>{
                if(err) throw err;
                let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;

                db.query(ql,(err,r)=>{
                    if(err) throw err;

                    res.send(this.model.update_packaging_callback(err,result));
                    
                    let message={
                        to:r[0].email,
                        subject:"Activation de compte Aladin",
                        html:"Votre produit a été approuvé."
                    };
                    
                    sendmail(message.to,message.subject,message.html);
                    
                });  

            });

        });
    }

    delete_pack_callback(req,res){
        db.query(this.model.delete_pack(req.params.id),(err,result)=>{
            res.send(this.model.delete_pack_callback(err,result));
        });
    }


}

module.exports = new packagingscontroller();


