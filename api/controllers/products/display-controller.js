var db = require('../../config/db-config')
var displays = require('../../models/products/display-model');
var order_date=require('../../config/order-date');

class displaysController{
    constructor(){
        this.model= new displays();
    }
/**
*  GET All displays
*/
    findAll_disp_callback(req,res){
        const limit=6
        const page= req.query.page
        const offset=(page-1)*limit

        db.query(this.model.findAll_disp(limit,offset),(err,result) =>{
            res.send(this.model.findAll_disp_callback(err,result,page));

        });

    }
/*
* GET One display
*/
    findOne_disp_callback(req,res){

        db.query(this.model.findOne_disp(req.params.id),(err,result)=>{
            res.send(this.model.findOne_disp_callback(err,result));
        });
    }

/**
* POST display 
*/
    create_disp_callback(req,res){
        const file = req.files.image;
        let date = order_date()
        file.mv("public/disps/"+file.name,(err,resp)=>{
            if(err)throw err

            var data=[[req.body.name,req.body.price,req.body.dim,req.body.ordered,date,"https://api.aladin.ci/disps/"+file.name,parseInt(req.body.staff),req.body.comment]]
            db.query(this.model.create_disp(),[data],(err,result)=>{

                res.send(this.model.create_display_callback(err,result));
            });
        })
    }

/**
*  Update disp products
*/
    update_cloth_callback(req,res){
        db.query(this.model.update_clt(req.body.ordered,req.params.id),(err,result)=>{

            if(err) throw err;
            let newsql=`SELECT user_id FROM aladin_displays WHERE disp_id=${req.params.id}`;
             
            db.query(newsql,(err,re)=>{
                if(err) throw err;
                let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;

                db.query(ql,(err,r)=>{
                    if(err) throw err;

                    res.send(this.model.update_clt_callback(err,result));
                    
                    let message={
                        to:r[0].email,
                        subject:"Activation de compte Aladin",
                        html:"Votre produit a été approuvé."
                    };
                    
                    sendmail(message.to,message.subject,message.html);
                    
                });  

            });

        });
    }

/**
* DELETE One display
*/
    delete_disp_callback(req,res){
        db.query(this.model.delete_disp(req.params.id),(err,result)=>{
            res.send(this.model.delete_disp_callback(err,result));
        });
    }


}
module.exports = new displaysController();
