const db = require('../../config/db-config');
const gadg_img = require('../../models/products/gadgets-img-model');
const order_date = require('../../config/order-date');

class gadget_imgController{

    constructor(){
        this.model= new gadg_img();
    }

/**
*  POST image gadget
*/
    create_img_gadg_callback(req,res){
        const filef = req.files.myfront;
        const fileb = req.files.myback;
        const filecf = req.files.mycfront;
        const filecb = req.files.mycback;
        let date = order_date()
        
        filef.mv('public/gadgets/'+filef.name,(err,resp)=>{
            if(err) throw err

            fileb.mv('public/gadgets/'+fileb.name,(err,rest)=>{
                if(err) throw err

                filecf.mv('public/gadgets/'+filecf.name,(err,reqs)=>{
                    if(err) throw err

                    filecb.mv('public/gadgets/'+filecb.name,(err,re)=>{
                        if(err) throw err

                        var data = [[date,req.body.gadget,"https://api.aladin.ci/gadgets/"+filef.name,"https://api.aladin.ci/gadgets/"+fileb.name,"https://api.aladin.ci/gadgets/"+filecf.name,"https://api.aladin.ci/gadgets/"+filecb.name]];
                        db.query(this.model.create_img_gadget(),[data],(erreur,myresult)=>{

                            res.send(this.model.create_img_gadget_callback(erreur,myresult));
                        });
                        

                    })
                })
            })
        }) 

    }


}
module.exports = new gadget_imgController();