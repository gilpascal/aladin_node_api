var db = require('../../config/db-config')
var gadgets = require('../../models/products/gadgets-model');
var order_date=require('../../config/order-date');

class gadgetsController{
   constructor(){
       this.model= new gadgets();
   }
/*
*GET All gadget 
*/ 
    findAll_gadg_callback(req,res){
        const limit=6
        const page= req.query.page
        const offset=(page-1)*limit

        db.query(this.model.findAll_gadg(limit,offset),(err,result)=>{
            res.send(this.model.findAll_gadg_callback(err,result,page));
        })
    }

/*
* GET One gadget
*/
   findOne_gadg_callback(req,res){

    db.query(this.model.findOne_gadg(req.params.id),(err,result)=>{
        res.send(this.model.findOne_gadg_callback(err,result));
    });
}

/**
* POST gadget 
*/
    create_gadg_callback(req,res){
        const file = req.files.image;
        let date = order_date();
        file.mv('public/gadgets'+file.name,(err,result)=>{
            if(err) throw err

            var data=[[req.body.name,req.body.price,req.body.ordered,date,"https://api.aladin.ci/gadgets/"+file.name,req.body.staff,req.body.comment]]
            db.query(this.model.create_gadget(),[data],(err,result)=>{

                res.send(this.model.create_gadget_callback(err,result));
            });
        })    
    }

/**
*  Update gagdet products
*/
    update_gad_callback(req,res){
        db.query(this.model.update_gadget(req.body.ordered,req.params.id),(err,result)=>{

            if(err) throw err;
            let newsql=`SELECT user_id FROM aladin_gadgets WHERE gadg_id=${req.params.id}`;
            
            db.query(newsql,(err,re)=>{
                if(err) throw err;
                let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;

                db.query(ql,(err,r)=>{
                    if(err) throw err;

                    res.send(this.model.update_gadget_callback(err,result));
                    
                    let message={
                        to:r[0].email,
                        subject:"Activation de compte Aladin",
                        html:"Votre produit a été approuvé."
                    };
                    
                    sendmail(message.to,message.subject,message.html);
                    
                });  

            });

        });
    }

/**
* DELETE gadget
*/
    delete_gadg_callback(req,res){
        db.query(this.model.delete_gadg(req.params.id),(err,result)=>{
            res.send(this.model.delete_gadg_callback(err,result));
        });
    }

}

module.exports= new gadgetsController();
