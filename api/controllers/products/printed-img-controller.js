const db = require('../../config/db-config');
const print_img = require('../../models/products/printed-img-model');
const order_date = require('../../config/order-date');

class printed_imgController{

    constructor(){
        this.model= new print_img();
    }

/**
*  POST image printed
*/
    create_img_print_callback(req,res){
        const filef = req.files.myfront;
        const fileb = req.files.myback;
        const filecf = req.files.mycfront;
        const filecb = req.files.mycback;
        let date = order_date();
        
        filef.mv('public/prints/'+filef.name,(err,resp)=>{
            if(err) throw err

            fileb.mv('public/prints/'+fileb.name,(err,rest)=>{
                if(err) throw err

                filecf.mv('public/prints/'+filecf.name,(err,reqs)=>{
                    if(err) throw err

                    filecb.mv('public/prints/'+filecb.name,(err,re)=>{
                        if(err) throw err

                        var data = [[date,req.body.print,"https://api.aladin.ci/prints/"+filef.name,"https://api.aladin.ci/prints/"+fileb.name,"https://api.aladin.ci/prints/"+filecf.name,"https://api.aladin.ci/prints/"+filecb.name]];
                        console.log(req.body)
                        db.query(this.model.create_img_printed(),[data],(erreur,myresult)=>{

                            res.send(this.model.create_img_printed_callback(erreur,myresult));
                        });
                        

                    })
                })
            })
        }) 

    }


}
module.exports = new printed_imgController();