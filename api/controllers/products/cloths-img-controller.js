const db = require('../../config/db-config');
const cloth_img = require('../../models/products/cloths-img-model');
const order_date = require('../../config/order-date');

class cloth_imgController{

    constructor(){
        this.model= new cloth_img();
    }

/**
*  POST image cloth
*/
    create_img_clt_callback(req,res){
        const filef = req.files.myfront;
        const fileb = req.files.myback;
        const filecf = req.files.mycfront;
        const filecb = req.files.mycback;
        let date = order_date();
        
        filef.mv('public/cloths/'+filef.name, (erreur,resp)=>{
            if(erreur) throw erreur

            fileb.mv('public/cloths/'+fileb.name, (erreur,rest)=>{
                if(erreur) throw erreur

                filecf.mv('public/cloths/'+filecf.name, (erreur,reqs)=>{
                    if(erreur) throw erreur

                    filecb.mv('public/cloths/'+filecb.name, (erreur,re)=>{
                        if(erreur) throw erreur

                        var data = [[date,req.body.cloth,"https://api.aladin.ci/cloths/"+filef.name,"https://api.aladin.ci/cloths/"+fileb.name,"https://api.aladin.ci/cloths/"+filecf.name,"https://api.aladin.ci/cloths/"+filecb.name]];
                        db.query(this.model.create_img_cloth(),[data],(erreur,myresult)=>{

                            res.send(this.model.create_img_cloth_callback(erreur,myresult));
                        });
                        

                    })
                })
            })
        }) 

    }


}
module.exports = new cloth_imgController();