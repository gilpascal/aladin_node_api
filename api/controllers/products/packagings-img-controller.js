var express = require('express');
const db = require('../../config/db-config');
const packagings_img = require('../../models/products/packagings-img-model');
const order_date = require('../../config/order-date');
var fileupload = require("express-fileupload");
var app = express();
app.use(fileupload());

class packagings_imgController{

    constructor(){
        this.model= new packagings_img();
    }

/**
*  POST image packaging
*/
    create_img_pack_callback(req,res){
        const filef = req.files.myfront;
        const fileb = req.files.myback;
        const filecf = req.files.mycfront;
        const filecb = req.files.mycback;
        let date = order_date();
        
        filef.mv('publ'+filef.name, (erreur,resp)=>{
            if(erreur) throw erreur
            
            fileb.mv('public/packs/'+fileb.name, (erreur,rest)=>{
                if(erreur) throw erreur

                filecf.mv('public/packs/'+filecf.name, (erreur,reqs)=>{
                    if(erreur) throw erreur

                    filecb.mv('public/packs/'+filecb.name, (erreur,re)=>{
                        if(erreur) throw erreur
                       
                        var data = [[date,req.body.pack,"https://api.aladin.ci/packs/"+filef.name,"https://api.aladin.ci/packs/"+fileb.name,"https://api.aladin.ci/packs/"+filecf.name,"https://api.aladin.ci/packs/"+filecb.name]];
                        console.log(data);
                        db.query(this.model.create_img_packaging(),[data],(erreur,myresult)=>{
                            
                            res.send(this.model.create_img_packaging_callback(erreur,myresult));
                        });
                        
                     
                    })
                })
            })
            
        }) 

    }

}
module.exports = new packagings_imgController();

