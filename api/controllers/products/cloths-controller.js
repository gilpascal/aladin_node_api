var db = require('../../config/db-config');
var order_date =require('../../config/order-date');
var cloths = require ('../../models/products/cloths-model');
const sendmail = require('../../config/mail/notify')

class clothsController{
    constructor(){
        this.model = new cloths();
            
    }
/**
* GET All Cloths 
*/   
    findAll_cloth_callback(req,res){ 
        const limit=6
        const page= req.query.page
        const offset=(page-1)*limit    
        db.query(this.model.findAll_clt(limit,offset),(err,result)=>{  

            res.send(this.model.findAll_clt_callback(err,result,response));
        });
    }

    findAll_clot_callback(req,res){ 
        const limit=6
        const page= req.query.page
        const offset=(page-1)*limit    
        db.query(this.model.findAll_cl(limit,offset),(err,result)=>{  

            res.send(this.model.findAll_cl_callback(err,result,response));
        });
    }

    findAll_clo_callback(req,res){ 
        const limit=6
        const page= req.query.page
        const offset=(page-1)*limit    
        db.query(this.model.findAll_c(limit,offset),(err,result)=>{ 

            res.send(this.model.findAll_c_callback(err,result,page));
        });
    }

/**
* GET One Cloth
*/   
    findOne_cloth_callback(req,res){

        db.query(this.model.findOne_clt(req.params.id),(err,result)=>{
            res.send(this.model.findOne_clt_callback(err,result));
        });
    }

/**
* POST cloth
*/
    create_cloth_callback(req,res){
        const file = req.files.img;
        let date = order_date();
        file.mv("public/cloths/"+file.name,(err,resp)=>{
            if(err) throw err

            var data=[[req.body.name,req.body.price,req.body.is_ordered,date,"https://api.aladin.ci/cloths/"+file.name,req.body.staff,req.body.comment,req.body.material,req.body.size,req.body.type_imp]]
            db.query(this.model.create_clt(),[data],(err,result)=>{
        
                res.send(this.model.create_clt_callback(err,result));
            });
        }) 
    }

/**
* UPDATE cloth
*/
    update_cloth_callback(req,res){
        db.query(this.model.update_clt(req.body.ordered,req.params.id),(err,result)=>{

            if(err) throw err;
            let newsql=`SELECT user_id FROM aladin_cloths WHERE clt_id=${req.params.id}`;
             
            db.query(newsql,(err,re)=>{
                if(err) throw err;
                let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;

                db.query(ql,(err,r)=>{
                    if(err) throw err;

                    res.send(this.model.update_clt_callback(err,result));
                    
                    let message={
                        to:r[0].email,
                        subject:"Activation de compte Aladin",
                        html:"Votre produit a été approuvé."
                    };
                    
                    sendmail(message.to,message.subject,message.html);
                    
                });  

            });

        });
    }

/**
* DELETE cloth
*/
    delete_cloth_callback(req,res){
        db.query(this.model.delete(req.params.id),(err,result)=>{
            res.send(this.model.delete_callback(err,result));
        });
    }
}

module.exports = new clothsController();