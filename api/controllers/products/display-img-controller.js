const db= require('../../config/db-config');
const disp_img = require('../../models/products/display-img-model');
const order_date= require('../../config/order-date');

class disp_imgController{

    constructor(){
        this.model= new disp_img();
    }

/**
* POST image display 
*/
    create_img_disp_callback(req,res){
        const filef = req.files.myfront;
        const fileb = req.files.myback;
        const filecf = req.files.mycfront;
        const filecb = req.files.mycback;
        let date = order_date();

        filef.mv('public/disps/'+filef.name,(err,resp)=>{
            if(err) throw err

            fileb.mv('public/disps/'+fileb.name,(err,rest)=>{
                if(err) throw err

                filecf.mv('public/disps/'+filecf.name,(err,reqs)=>{
                    if(err) throw err

                    filecb.mv('public/disps/'+filecb.name,(err,re)=>{
                        if(err) throw err

                        var data = [[date,req.body.disp,"https://api.aladin.ci/disps/"+filef.name,"https://api.aladin.ci/disps/"+fileb.name,"https://api.aladin.ci/disps/"+filecf.name,"https://api.aladin.ci/disps/"+filecb.name]];
                        db.query(this.model.create_img_display(),[data],(erreur,myresult)=>{

                            res.send(this.model.create_img_display_callback(erreur,myresult));
                        });
                        

                    })
                })
            })
        })            

    }
}
module.exports = new disp_imgController();