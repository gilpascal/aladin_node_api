var db = require('../../config/db-config');
var printed = require('../../models/products/printed-model');
var order_date=require('../../config/order-date');

class printedcontroller{
    constructor(){
       this.model= new printed();
    }
/**
*GET All printed 
*/
    findAll_print_callback(req,res){
        const limit=6
        const page= req.query.page
        const offset=(page-1)*limit
        db.query(this.model.findAll_print(limit,offset),(err,result)=>{
            res.send(this.model.findAll_print_callback(err,result,page));
        })
    }

/**
* GET One printed
*/
    findOne_print_callback(req,res){

        db.query(this.model.findOne_print(req.params.id),(err,result)=>{
            res.send(this.model.findOne_print_callback(err,result));
        });
    }

/**
*  POST printed
*/
    create_print_callback(req,res){
        const file = req.files.image;
        let date=order_date();
        file.mv('public/prints/'+file.name,(err,result)=>{
            if(err) throw err

            var data=[[req.body.name,req.body.price,req.body.gram,req.body.ordered,date,"https://api.aladin.ci/prints/"+file.name,req.body.staff,req.body.comment,req.body.pellicule,req.body.volet]]
            db.query(this.model.create_printed(),[data],(err,result)=>{

                res.send(this.model.create_printed_callback(err,result));
            });
        })    
    }

/**
*  Update packaging products
*/
update_print_callback(req,res){
    db.query(this.model.update_printed(req.body.ordered,req.params.id),(err,result)=>{

        if(err) throw err;
        let newsql=`SELECT user_id FROM aladin_printed WHERE print_id=${req.params.id}`;
        
        db.query(newsql,(err,re)=>{
            if(err) throw err;
            let ql=`SELECT email FROM aladin_users WHERE user_id=${re[0].user_id}`;

            db.query(ql,(err,r)=>{
                if(err) throw err;

                res.send(this.model.update_printed_callback(err,result));
                
                let message={
                    to:r[0].email,
                    subject:"Activation de compte Aladin",
                    html:"Votre produit a été approuvé."
                };
                
                sendmail(message.to,message.subject,message.html);
                
            });  

        });

    });
}

/**
* DELETE printed
*/
    delete_print_callback(req,res){
        db.query(this.model.delete_print(req.params.id),(err,result)=>{
            res.send(this.model.delete_print_callback(err,result));
        });
    }


}

module.exports = new printedcontroller();

