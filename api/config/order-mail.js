
function ordermail(data){
   
    html=`<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>Aladin</title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <style>
            @import url('https://fonts.googleapis.com/css?family=Poppins:400,700,900');
        
     *{
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    
     }
    
     h1{
       position: relative;
       top: 80px;
       text-align: center;
       color: white;
       font-family: 'Poppins' ;
       font-size: 25px;
     }
    
    .btn{
      bottom: 50px;
      width: 20%;
      padding: 10px;
      background: "#324161";
      color: white;
      right: 41%;
      border-radius: 10px;
      font-size: 20px;
      font-family: 'Poppins';
    }
    .main-head{
      background: #324161;
      height: 15%;
      padding: 1%;
      text-align: center;
    
    }
    
    .hero{
      margin-bottom: 10px;
    }
    
    .image{
      margin-top: 12%;
      text-align: center;
      margin-bottom: 10px;
    }
    
    .image img{
      height: 250px;
    
    }
    
    .bouton{
      position: relative;
      text-align: center;
    }
    
    @media screen and (max-width:500px) {
      h1 {
        font-size: 15px;
      } 
      .image img{
        height: 180px;
      }
      .image{
        margin-top: 24%;
      }
      .btn{
        width: 80%;
      }
    }
           </style>
        
        </head>
        <body style="background-color: #fab91a;"> 
            <header  style="background: #324161;
            height: 15%;
            padding: 1%;
            text-align: center;">
              <img src="http://api.aladin.ci/images/logoblanc.png" height="35"> 
            </header> 
    
            <div class="container hero">
              <p>
                <h1>Félicitation ${data.name} votre commande a été effectué avec succès</h1>
              </p>
            </div>
            <div class="container">
              <div style=" margin-top: 12%;
              text-align: center;
              margin-bottom: 10px;">
                 <img src="http://api.aladin.ci/images/create_partner.png" height="250px;">
              </div>
            </div> 
            <div class="container " style="  position: relative;
            text-align: center;">
              <button class="btn btn-lg" href="http://aladin.ci" style=" bottom: 50px;
              width: 20%;
              padding: 10px;
              background: "#324161";
              color: white;
              right: 41%;
              border-radius: 10px;
              font-size: 20px;
              font-family: 'Poppins';"><h5>Rendez-Vous sur www.aladin.ci</h5></button>
            </div>
        </body>
    </html>`;
    
    return html
    
    }
    
    
    
    module.exports =ordermail