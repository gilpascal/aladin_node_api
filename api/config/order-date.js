let order_date =()=>{
    let ts = Date.now();
    let date_ob = new Date(ts);
    let date = date_ob.getDate();
    let month = date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();
    let hour=date_ob.getHours()
    let min = date_ob.getMinutes()
    let s = date_ob.getSeconds()
    
   return year + "-" + month + "-" + date + '-' + hour + ":" + min + ":" + s;
}

//console.log(order_date())

module.exports=order_date;

