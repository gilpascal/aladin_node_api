const bcrypt=require("bcrypt");

var Hash=async (password) =>{
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(password, salt)
    console.log(hash)
}

exports.module=Hash;