
module.exports = class shapes{
    
    constructor(){
        this.table="aladin_shapes";
        this.fiels={id:'shap_id',name:'name_shape',url:'url',created_at:'created_at',deleted_at:'deleted_at',updated_at:'updated_at'};
    }

/**Tout tester Ok */
/**
 * Get All shapes
 */
    findAll_shp(limit,offset){
        
        var sql=`SELECT * FROM aladin_shapes LIMIT ${limit} OFFSET ${offset}`
        return sql;
    }

    findAll_shp_callback(err,result){
        if(err){
            return{
                error:err
            }
        }
        return{
            status:200,
            data:result
        }
    }

/**
* Post Shape
*/
   create_shp(){
        let sql = `INSERT INTO ${this.table} (url, name,created_at) VALUES ?`;
        return sql;

    }

    create_shp_callback(err,result){
       if(err){
            return{
                status:400,    
                error:err,
                message:"bad request"
            }
        }
        return{
            status:201,
            message:"success",
            data:result
        }
    }


/**
*Get Single shape
*/
    findOne_shape(id){
        
        let sql = `SELECT * FROM ${this.table} WHERE ${this.fields.id}=${id}`;
        return sql;
    }

    findOne_shp_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            status:200,
            shape:result
        }
    }
}