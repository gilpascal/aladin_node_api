const { token } = require("morgan");


module.exports = class users {
    
    constructor(){
        this.table="aladin_users";
        this.fields={id:"user_id",nom:"name",address:"email",pwd:"password",phone:"phone",city:"city",is_partner:"is_partner",whatssap:"whatssap",created:"created",activated:"activated"};
    }


/** 
 * SIGN UP User
*/
    create_account_user(){
       let sql= `INSERT INTO ${this.table} (name,email,password,phone,created,city,is_partner,whatssap,country) VALUES ?`;
       return sql;
       
    }
    create_account_user_callback(err,token){
        if(err){
            return{
                status:false,
                message:"La creation de compte a échouée",
                error:err
            }
        }
        return{
            status:true,
            token:token,
            message:"Création de compte reussi!!!"
            
            

        }
              
    }

/** 
* LOGIN User
*/
   login_user(){
        let sql=`SELECT * FROM ${this.table} WHERE email like ? LIMIT 1`;
        return sql 
    }

    login_user_callback(err,resultat,result,token){
        if(err){
            return{
                status:false,
                error:err
                
                
            }
        }
        if(resultat.length>0){

            if(result==true){
                return{
                    status:true,
                    token:token,
                    message:"Authentification reussi",
                    user:{id:resultat[0].user_id,is_partner:resultat[0].is_partner}
                }
                
            }else{
                return{
                    status:false,
                    message:"Authentification echoué mot de passe introuvable",
                    
                }
            }
        }else{
            return{
                status:false,
                message:"Authentification echoué email introuvable",
                data:resultat
            }
            
        }
    }



/** 
* Find All User
*/
    findAll_user(){
        return `SELECT * FROM ${this.table} WHERE is_partner=${0}`
    }

    findAll_user_callback(err,result){
        if(err){
            return{
                status:false,
                message:"An error occured",
                error:err
            }
        }
        return{
            status:true,
            message:"Retrieve All with success",
            data:result
        }
    }

/** 
 * Find One User
*/
    findOne_user(id){
        return `SELECT * FROM ${this.table} WHERE  ${this.fields.id}=${id} AND is_partner=${0}`
    }

    findOne_user_callback(err,result){
        if(err){
            return{
                status:false,
                message:"An error occured",
                error:err
            }
        }
        return{
            status:true,
            message:"Retrieve one with success",
            data:result
        }
    }

/** 
* Delete One User
*/
    delete_user(id){
        return `DELETE FROM ${this.table} WHERE  ${this.fields.id}=${id} AND is_partner=${0}`
    }

    delete_user_callback(err,result){
        if(err){
            return{
                status:false,
                message:"An error occured",
                error:err
            }
        }
        return{
            status:true,
            message:"Deleted with success",
            data:result
        }
    }

/** 
* RESET pwd User
*/
    reset_password(pwd,id){
        let sql =`UPDATE ${this.table} SET ${this.fields.pwd}= ${pwd} WHERE ${this.fields.id}=${id}`;
        return sql;
    }

    reset_password_callback(err,result){
        if(err){
            return{
     
                status:false,
                message:"Mauvaise requete",
                eror:err
            }
        }
        return{
            status:true,
            message:"Mot de passe réinitialisé",
            data:result
        }
    }

/**
* Change Pwd 
*/
    change_password(id){
        let sql=`SELECT * FROM ${this.table} WHERE ${this.fields.id}=${id} LIMIT 1`;
        return sql;
    }

    change_password_callback(err,resul){
        if(err){
            return{
                status:false,
                error:err
            }
        }

        if(resul){

            if(err){
                return{
                    status:false,
                    error:err
                }
            }else{
                return{
                    status:true,
                    message:"Opération Réussie"
                }
            }

        }else{
            return{
                status:false,
                message:"ancien mot de passe introuvable"
                
            }
        }
            
    }

/** 
* Update details User ***A revoir
*/
    update_details(nom,address,phone,id){

        console.log(nom,address,phone,id)
        let sql=`UPDATE ${this.table} SET ${this.fields.nom}=${nom}, ${this.fields.address}=${address}, ${this.fields.phone}=${phone} WHERE ${this.fields.id}=${id}`;
        return sql;
    }

    update_details_callback(err,resul){
        if(err){
            return {
                status:false,
                code:400,
                error:err
            }
        }
        return{
            status:true,
            code:200,
            data:resul
        }
    }


/** 
 * Checking Mail 
*/
   checking_mail(){
        return `SELECT name,email FROM ${this.table} WHERE email like ?`;
        
   }

   checking_mail_callback(err,result){
        if(err){
            return{
            error:err,
            status:false,
            message:"ERREUR BAD REQUEST"
            };
        }
        if(result.length>0){
            let user={
                id:result[0].user_id,
                status:true,
                message:"OOK"
            }
            return{user:user};
        }else{
            return{
                status:false,
                message:"Email introuvalbe"
            }
        }

    }


    
    
}
