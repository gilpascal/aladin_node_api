var db = require('../config/db-config');
var express = require('express');

module.exports = class tops{

    constructor(){
        this.table="aladin_tops";
        this.fields={id:"id",img:"img",price:"price",user_id:"user_id",comment:"comment"};
    }

/**
* Get All tops
*/

    findAll_tops(){
        return `SELECT * FROM ${this.table}`;

        
    }

    findAll_tops_callback(err,result){
        if(err){
            return{
                status:false,
                message:'Failed',
                error: err
            }
        }
        return{
            status:true,
            message:'Success',
            data: result
        }
    }

/**
* Get single top
*/
    findOne_tops(id){
        let sql = `SELECT * FROM ${this.table} WHERE ${this.fields.id}=${id}`;
        return sql; 
    }

    findOne_tops_callback(err,result){
        if(err){
            return{
                status:false,
                message:'Failed',
                error:err
            }
        }
        return{
            status:true,
            message:'Success',
            data:result
        }
    }
/**
* Post top
*/
    create_tops(){
        let sql=`INSERT INTO ${this.table} (img,price,user_id,comment) VALUES ? `;
        return sql; 
    }

    create_tops_callback(err,result){
        if(err){
            return{
                status:false,
                message:"Mauvais format des données ou champ nom reseignés",
                error:err
            }
        }
        return{
            status:true,
            message:"Success",
            data:result
        }
    }
}
