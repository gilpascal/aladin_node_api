
module.exports= class colors{

    constructor(){
        this.table="aladin_colors";
        this.fields={id:"id",lib:"lib",cloth:"cloth",print:"print",gadget:"gadget",pack:"pack",disp:"disp",created_at:"created-at",lib_img:"lib_img",lib_back:"lib_back"};
    }
/** Tout testé Ok */
/**
 * POST color cloth
 */    
    create_color_clt(){
        let sql = `INSERT INTO aladin_colors(lib_img,lib_back,lib,cloth,created_at) VALUES ?`;
        return sql;
    }

    create_color_clt_callback(err,result){
        if(err){
            
            return{
                status:false,
                message:"une erreur s'est produite verifiez le format des données ",
                error:err,
            }
            
        }
        return{
            status:true,
            message:"Les données ont bien étés enregistrées",
            data:result
        }
    }

/**
 * POST color display
 */ 
    create_color_disp(){
        let sql = `INSERT INTO aladin_colors(lib_img,lib_back,lib,disp,created_at) VALUES ?`;
        return sql;
    }

    create_color_disp_callback(err,result){
        if(err){
            
            return{
                status:false,
                message:"une erreur s'est produite verifiez le format des données ",
                error:err,
            }
            
        }
        return{
            status:true,
            message:"Les données ont bien étés enregistrées",
            data:result
        }
    }

/**
 * POST color gadget
 */ 
    create_color_gadg(){
        let sql = `INSERT INTO aladin_colors(lib_img,lib_back,lib,gadget,created_at) VALUES ?`;
        return sql;
    }

    create_color_gadg_callback(err,result){
        if(err){
            
            return{
                status:false,
                message:"une erreur s'est produite verifiez le format des données ",
                error:err,
            }
            
        }
        return{
            status:true,
            message:"Les données ont bien étés enregistrées",
            data:result
        }
    }

/**
 * POST color packaging
 */ 
    create_color_pack(){
        let sql = `INSERT INTO aladin_colors(lib_img,lib_back,lib,pack,created_at) VALUES ?`;
        return sql;
    }

    create_color_pack_callback(err,result){
        if(err){
            
            return{
                status:false,
                message:"une erreur s'est produite verifiez le format des données ",
                error:err,
            }
            
        }
        return{
            status:true,
            message:"Les données ont bien étés enregistrées",
            data:result
        }
    }

/**
 * POST color print
 */ 
    create_color_print(){
        let sql = `INSERT INTO aladin_colors(lib_img,lib_back,lib,print,created_at) VALUES ?`;
        return sql;
    }

    create_color_print_callback(err,result){
        if(err){
            
            return{
                status:false,
                message:"une erreur s'est produite verifiez le format des données ",
                error:err,
            }
            
        }
        return{
            status:true,
            message:"Les données ont bien étés enregistrées",
            data:result
        }
    }
}
    