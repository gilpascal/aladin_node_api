module.exports = class partner{

    constructor(){
        this.table="aladin_users";
        this.fields={id:"user_id",name:"name",email:"email",password:"password",contact:"phone",city:"city",is_partner:"is_partner",whatssap:"whatssap",created:"created",activated:"activated"};
    }
/**
 * Partner details
 */   

    partner_details(){
        return `SELECT aladin_users.user_id,aladin_users.name,aladin_users.email,aladin_users.country,aladin_users.created,aladin_users.activated,aladin_partners_account.pmode,aladin_partners_account.accountnmb,aladin_partners_account.bcode,aladin_partners_account.ibn,aladin_partners_account.mobile FROM aladin_partners_account  JOIN aladin_users ON aladin_partners_account.user_id=aladin_users.user_id `
    }
    partner_details_callback(err,result){
        if(err){
            return{
                status:false,
                message:"Failed",
                error:err
            }
        }
        return{
            status:true,
            message:"Success",
            detail:result
        }
    }

/**
 * 
 */
    partner_valid_account(){
        return `SELECT ${this.table}.user_id,${this.table}.name,${this.table}.email,${this.table}.country,${this.table}.created,${this.table}.activated,aladin_partners_firms.sr FROM aladin_partners_firms  JOIN aladin_users ON aladin_partners_firms.user_id=aladin_users.user_id `
    }
    
    partner_valid_account_callback(err,result){
        if(err){
            return{
                status:false,
                message:"Failed",
                error:err
            }
        }
        return{
            status:true,
            message:"Success",
            partner:result
        }
    }

/** 
* PUT activated
*/
    update_activate(activated,id){
        let sql =`UPDATE aladin_users SET activated=${activated} WHERE user_id=${id}`
        return sql;      
    }

    update_activate_callback(err,result,id,r){

        if(err){
            return{
                status:false,
                error:err
            }
        }
        
    }
}