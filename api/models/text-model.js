module.exports= class texts{

    constructor(){
        this.table="aladin_texts";
        this.fields={id:"id",is_back:"is_back",data:"data",image:"image"} ;
    }

/** Tout testé Ok */
/**
 * POST Text 
 */
    create_text(){
        let sql= `INSERT INTO ${this.table} (data,image,is_back) VALUES ?`;
        return sql;
    }

    create_text_callback(err,re){
        if(err){
            return{
                status:false,
                message:"mauvaise requete",
                error:err
            }
        }
        return{
            status:true,
            message:"text enregistré",
            data:re
        }
    }

/**
 * GET Text by id
 */
    findOne_text(id){
        let sql=`SELECT * FROM ${this.table} WHERE image= ${id}`;
        return sql;
    }

    findOne_text_callback(err,result){
        if(err){
            return{
                status:false,
                message:"Erreur de la requete",
                error:err
            }
        }
        
        return{
            status:true,
            message:"Success",
            data:result
        }
    }
}