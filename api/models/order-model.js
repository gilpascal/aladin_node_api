
module.exports = class order{
  
    constructor(){  
        this.table="aladin_orders";
        this.fields={id:"ord_id",status:"status",customer:"customer",created:"created_at",updated:"updated_at",deleted:"deleted_at",dmode:"dmode",d_place:"d_place",total:"total",city:"city"}
    }

/** Tout testé Ok */
/**
* POST  orders
*/
    create_order(){
        let sql= `INSERT INTO aladin_orders(status,customer,created_at,dmode,d_place,total,city) VALUES ?`;
        return sql;

    }

    create_order_callback(err,result){
        if(err){
            return{
                code:400,
                status:false,
                error:err
                        
            }
        }
        return{
            code:201,
            status:true,
            message:"commande ajoutée",
            resp:result       
        }
    }


    

/**
* GET All pending orders
*/
    findAll_pending(){
        let sql= `SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at ,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
        INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
        INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
        WHERE status like ?`;
        return sql;

    }

    findAll_pending_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            status:true,
            data:result
        }
    }

/**
* GET All order customer 
*/
    findAll_order_customer(user){
        
        let sql=`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
        INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
        INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
        WHERE aladin_orders.customer=${user}`;
        return sql;

    }

    findAll_order_customer_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            status:true,
            message:"reçu",
            data:result
        }
    }
   
        
/**
* GET All order delivered 
*/
    findAll_delivered(){
        let sql=`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at ,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
        INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
        INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
        WHERE status like ?`;
        return sql;

    }

    findAll_delivered_callback(err,r){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            status:true,
            data:r
        }
    }

/**
* GET All order rejected 
*/
    findAll_rejected(){
        let sql=`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at ,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
        INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
        INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
        WHERE status like ?`;
        return sql;

    }

    findAll_rejected_callback(err,result){
        if(err){
            return{
                status:false,
                message:"erreur mauvaise requete",
                error:err
            }
        }
        return{
            status:true,
            message:"Succès",
            data:result
        }
    }

/**
* GET All order new 
*/
    findAll_news(){
        let sql=`SELECT aladin_orders.ord_id,aladin_orders.status,aladin_users.name,aladin_users.email,aladin_users.phone,aladin_orders.dmode,aladin_orders.created_at,aladin_product_ordered.description, aladin_orders.total,aladin_orders.city,aladin_orders.d_place FROM aladin_orders
        INNER  JOIN aladin_users ON aladin_orders.customer=aladin_users.user_id
        INNER JOIN aladin_product_ordered ON aladin_product_ordered.orders=aladin_orders.ord_id
        WHERE status like ?`;
        return sql;

    }

    findAll_news_callback(err,result){
        if(err){
            return{
                status:false,
                message:"erreur mauvaise requete",
                error:err
            }
        }
        return{
            status:true,
            message:"Succès",
            data:result
        }
    }


/**
* UPDATE order status pending
*/
    update_status_pending(id){
        let sql =`UPDATE ${this.table} SET ${this.fields.status}=? WHERE ${this.fields.id}=${id}`;
        return sql;
    }

    update_status_pending_callback(err,result){
        if(err){
            return {
                status:false,
                message:err.sqlMessage,
                error:err
            }
        }
        return{
            status:true,
            message:"commande mise en mode de production",
            res:result
        }
    }

/**
* UPDATE  order status Ok
*/
    update_status_Ok(id){
        let sql =`UPDATE ${this.table} SET ${this.fields.status}=? WHERE ${this.fields.id}=${id}`;
        return sql;
    }

    update_status_Ok_callback(err,result){
        if(err){
            return {
                status:false,
                message:err.sqlMessage,
                error:err
            }
        }
        return{
            status:true,
            message:"commande mise en mode de production",
            res:result
        }
    }


/**
* UPDATE  order status rejected
*/
    update_status_rejected(id){
        let sql =`UPDATE ${this.table} SET ${this.fields.status}=? WHERE ${this.fields.id}=${id}`;
        return sql;
    }

    update_status_rejected_callback(err,result){
        if(err){
            return {
                status:false,
                message:err.sqlMessage,
                error:err
            }
        }
        return{
            status:true,
            message:"commande mise en mode de production",
            res:result
        }
    }

    

    

}