
module.exports= class clipart {

    constructor(){
        this.table="aladin_cliparts";
        this.fields={id:"clip_id",name:"name",url:"url",created:"created_at",deleted:"deleted_at",updated:"updated_at"};
    }

    /**Tout testé Ok */
/**
 * POST clipart
 */
    create_clipart(){
        let sql = `INSERT INTO ${this.table} (url, name,created_at) VALUES ?`;
        return sql;
    }

    create_clipart_callback(err,result){
        if(err){
            return{
                status:400,
                message:"bad request",
                error:err,
            }
            
        }    
        return{ 
            status:201,
            message:"success",
            data:result,
               
        }
        
        
    }

/**
 * GET All uploaded clips A revoir
 */
    findAll_cliparts(limit,offset){
        let sql = `SELECT * FROM ${this.table} LIMIT ${limit} OFFSET ${offset}`;
        return sql;
    }

    findAll_cliparts_callback(err,result){
        if(err){
            return{
                status:400,
                error:err
            }
        }
        return{
            status:200,
            cliparts:result
        }

    }

/**
 * GET One clipart
 */
    findOne_clipart(id){
        let sql = `SELECT * FROM ${this.table} WHERE ${this.fields.id}=${id}`;
        return sql;
    }

    findOne_clipart_callback(err,result){
        if(err){
            return{
                status:400,
                error:err
            }
        }
        return{
            status:200,
            data:result
        }
    }
}