
module.exports= class editcolors{

    constructor(){
        this.table="aladin_editor_colors";
        this.fields={id:"id",lib:"lib",staff:"staff"};
    }
/**Tout tester Ok */
/**
 * GET Color
 */
    findAll_edit_color(){
        let sql=`SELECT*FROM ${this.table}`;
        return sql;
    }

    findAll_edit_color_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            status:true,
            data:result
        }
    }

/**
 * POST Color
 */
    create_edit_color(){
        let sql=`INSERT INTO ${this.table} (lib,staff) VALUES ?`;
        return sql;
    }

    create_edit_color_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            status:true,
            data:result
        }
    }
}