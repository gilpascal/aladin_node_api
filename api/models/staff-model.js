
module.exports = class staf {
    
    constructor(){
        this.table="aladin_staff";
        this.fields={id:"id",name:"name",role:"role",created_at:"created_at",email:"email",pwd:"password",phone:"phone"};
    }


/** 
* SIGN UP staff
*/
    create_account_staff(){
       let sql= `INSERT INTO ${this.table} (name,role,email,password,phone,created_at) VALUES ?`;
       return sql ;
       
    }
    create_account_staff_callback(err,result,token){
        if(err){
            return{
                status:false,
                message:"mauvais format des données ou email déjà utilisé",
                error:err
            }
        }
        return{
            status:true,
            message:"Vous avez crée un utilisateur!!!",
            data:result,
            token:token
        }
              
    }

/** 
* LOGIN staff A revoir
*/
    login_staff(){
        let sql=`SELECT * FROM aladin_staff WHERE email like ? LIMIT 1`;
        return sql 
    }

    login_staff_callback(err,result,resultat,token){
        if(err){
            return{
                status:false,
                error:err
                
                
            }
        }
        if(resultat.length>0){

            if(result==true){
                return{
                    status:true,
                    message:"Authentification reussi",
                    user:{id:resultat[0].id,role:resultat[0].role},
                    token:token
                }
                
            }else{
                return{
                    status:false,
                    message:"Authentification echoué mot de passe introuvable",
                    
                }
            }
        }else{
            return{
                status:false,
                message:"Authentification echoué email introuvable",
                data:resultat
            }
            
        }
    }

/** 
* Find All staff
*/
    findAll_staff(){
      let sql= `SELECT name,email,phone,role FROM aladin_staff LIMIT 100`;
      return sql;
    }

    findAll_staff_callback(err,resp){
        if(err){
            return{
                status:false,
                message:"An error occured",
                error:err
            }
        }
        if(resp.length>0){
            return{
              status:true,
              users:resp
            }
        }else{
            return{
              status:true,
              message:"aucun objets trouvé"
            }
        }
    }

/** 
* Find staff by role (designer)
*/
    findAll_staff_designer(){
        return `SELECT * FROM ${this.table} WHERE role='designer'`
    }

    findAll_staff_designer_callback(err,result){
        if(err){
            return{
                status:false,
                message:"An error occured",
                error:err
            }
        }
        return{
            status:true,
            message:"Retrieve designer with success",
            data:result
        }
    }

/** 
* Find staff by role (conseiller client)
*/
    findAll_staff_cc(){
        return `SELECT * FROM ${this.table} WHERE role='conseiller clients'`
    }

    findAll_staff_cc_callback(err,result){
        if(err){
            return{
                status:false,
                message:"An error occured",
                error:err
            }
        }
        return{
            status:true,
            message:"Retrieve conseiller clients with success",
            data:result
        }
    }
/** 
* Find staff by role (admin)
*/
    findAll_staff_admin(){
        return `SELECT * FROM ${this.table} WHERE role='admin'`
    }

    findAll_staff_admin_callback(err,result){
        if(err){
            return{
                status:false,
                message:"An error occured",
                error:err
            }
        }
        return{
            status:true,
            message:"Retrieve admin with success",
            data:result
        }
    }

/** 
 * Delete One staff
*/
delete_staff(id){
    return `DELETE FROM ${this.table} WHERE  ${this.fields.id}=${id} `
}

delete_staff_callback(err,result){
    if(err){
        return{
            status:false,
            message:"An error occured",
            error:err
        }
    }
    return{
        status:true,
        message:"Deleted staff with success",
        data:result
    }
}

/**
* Change staff password
*/
    change_pwd(pwd,id){
      let sql =`UPDATE ${this.table} SET ${this.fields.pwd}=${pwd} WHERE ${this.fields.id}=${id}`;
      return sql;
    }

    change_pwd_callback(err,result){
        if(err){
            return{
                status:false,
                message:"Mauvais format des données",
                error:err
            }
        }
        return{
            status:true,
            message:"succès",
            data:result
        }
    }

/**
* Reset password
*/
    reset_password_staff(pwd,id){
        let sql =`UPDATE ${this.table} SET ${this.fields.pwd}=${pwd} WHERE ${this.fields.id}=${id}`;
        return sql;
    }

    reset_password_staff_callback(err,result){
        if(err){
            return{
                status:false,
                message:"Mauvais format des données",
                error:err
            }
        }
        return{
            status:true,
            message:"succès",
            data:result
        }
    }
    
}

    


