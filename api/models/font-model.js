
module.exports = class fonts{
  
    constructor(){  
        this.table="aladin_fonts";
        this.fields={id:"font_id",name:"name",url:"url",created:"created_at"}
    }

/**
 * POST font
 */
    create_font(){
        let sql = `INSERT INTO aladin_fonts(url, name,created_at) VALUES ?`;
        return sql
    }

    create_font_callback(err,result){

        if(err){
            return{
                status:400,
                message:"bad request",
                error:err
                                
            }
            
        }
        return{
            status:201,
            message:"success",
            data:result

        }
    }

/**
 * GET font
 */
    findAll_font(){
        let sql = `SELECT * FROM aladin_fonts `;
        return sql
    }

    findAll_font_callback(err,result){

        if(err){
            return{
                status:400,
                error:err
                                
            }
            
        }
        return{
            status:200,
            fonts:result

        }
    }

/**
 * GET font
 */
     findOne_font(id){
        let sql = `SELECT * FROM aladin_fonts WHERE font_id=${id} `;
        return sql
    }

    findOne_font_callback(err,result){

        if(err){
            return{
                status:400,
                error:err
                                
            }
            
        }
        return{
            status:200,
            fonts:result

        }
    }

}

