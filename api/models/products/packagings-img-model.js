module.exports = class packaging_img{

    constructor(){
        this.table="aladin_images";
        this.fields={id:"id",myfront:"front_side",pack:"pack",created_at:"created_at",myback:"back_side",mycfront:"customimg",mycback:"custimg"};
    }
/**Tout testé Ok */
/**
* POST image packaging 
*/
    create_img_packaging(){
        let sql=`INSERT INTO ${this.table} (created_at,pack,front_side,back_side,customimg,custimg) VALUES ?`;
        return sql;
    }

    create_img_packaging_callback(erreur,myresult){
        if(erreur){
            return{

                status:false,
                message:"une erreur s'est produite verifiez le format des donnée",
                error:erreur
            }
        }
        return{

            status:true,
            message:'Les données ont bien été enrégistrées',
            data:myresult
 
        }

    }
}

