module.exports = class printed_img{

    constructor(){
        this.table="aladin_images";
        this.fields={id:"id",myfront:"front_side",print:"print",created_at:"created_at",myback:"back_side",mycfront:"customimg",mycback:"custimg"};
    }
/** Tout payer */
/**
* POST image printed 
*/
    create_img_printed(){
        let sql=`INSERT INTO ${this.table} (created_at,print,front_side,back_side,customimg,custimg) VALUES ?`;
        return sql;
    }

    create_img_printed_callback(erreur,myresult){
        if(erreur){
            return{

                status:false,
                message:"une erreur s'est produite verifiez le format des donnée",
                error:erreur
            }
        }
        return{

            status:true,
            message:'Les données ont bien été enrégistrées',
            data:myresult
 
        }

    }
}

