module.exports = class gadget_img{

    constructor(){
        this.table="aladin_images";
        this.fields={id:"id",myfront:"front_side",gadget:"gadget",created_at:"created_at",myback:"back_side",mycfront:"customimg",mycback:"custimg"};
    }
/** Tout testé Ok */
/**
* POST image gadget 
*/
    create_img_gadget(){
        let sql=`INSERT INTO ${this.table} (created_at,gadget,front_side,back_side,customimg,custimg) VALUES ?`;
        return sql;
    }

    create_img_gadget_callback(erreur,myresult){
        if(erreur){
            return{

                status:false,
                message:"une erreur s'est produite verifiez le format des donnée",
                error:erreur
            }
        }
        return{

            status:true,
            message:'Les données ont bien été enrégistrées',
            data:myresult
 
        }

    }
}

