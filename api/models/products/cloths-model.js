
module.exports = class cloth { 

    constructor(){ 
        
        this.table="aladin_cloths"; 
        
        this.fields={id:"clt_id",name:"name_cloths",price:"price",ordered:"is_ordered",date:"created_at",image:"img",staff:"staff",comment:"comment",mat:"material",size:"size",type:"type_imp"};

        
    }
/**Tout tester Ok */
/**
 * Get All Cloths products
 */

    findAll_clt(limit,offset){
        let sql= `SELECT * FROM aladin_cloths WHERE is_ordered=${1} LIMIT ${limit} OFFSET ${offset}`;
        return sql;
         
    }

    findAll_clt_callback(err,result,page){
        if(err){
            return {
                status:false,
                message:"failed",
                error: err
            }
        }
        return {
            status:true,
            page_number:page,
            products_page_number:result.length,
            data:result
        }
        
    }

    findAll_cl(limit,offset){
        let sql= `SELECT * FROM aladin_cloths WHERE is_ordered=${1} LIMIT ${limit} OFFSET ${offset}`;
        return sql;
         
    }

    findAll_cl_callback(err,result){
        if(err){
            return {
                status:false,
                message:"failed",
                error: err
            }
        }
        return {
            status:true,
            page_number:page,
            products_page_number:result.length,
            data:result
        }
        
    }

    findAll_c(limit,offset){
        let sql= `SELECT * FROM aladin_cloths WHERE is_ordered=${1} LIMIT ${limit} OFFSET ${offset}`;
        return sql;
         
    }

    findAll_c_callback(err,result,response){
        if(err){
            return {
                status:false,
                message:"failed",
                error: err
            }
        }
        return {
            status:true,
            page_number:page,
            products_page_number:result.length,
            data:result
        }
        
    }

/**
 * Post Cloth product
 */
    create_clt(){
        let sql= `INSERT INTO ${this.table} (name_cloths,price,is_ordered,created_at,img,staff,comment,material,size,type_imp) values ? `;
        return sql;
       
    }

    create_clt_callback(err,result){
        
        if(err){
            return{
                status:false,
                message:"Mauvais format des données ou champ nom renseignés",
                error:err
            }
        }
        return{
            status:true,
            message:"Les données ont bien été enregistrées!",
            data:result
        } 

    }
    


/**
 * Get single Cloth product
 */

    findOne_clt(id){
        let sql = `SELECT aladin_cloths.clt_id,aladin_cloths.price,aladin_cloths.color,aladin_cloths.mode,aladin_cloths.size,aladin_cloths.material,aladin_cloths.type_imp,aladin_cloths.img,aladin_cloths.comment,aladin_cloths.staff,aladin_cloths.user_id,aladin_cloths.name_cloths,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.data,aladin_texts.is_back,aladin_colors.lib_img,aladin_colors.lib,aladin_colors.lib_back FROM aladin_images
        JOIN aladin_cloths ON aladin_images.cloth=aladin_cloths.clt_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        JOIN aladin_colors ON aladin_colors.cloth=aladin_cloths.clt_id
        WHERE aladin_cloths.clt_id=${id}`;
        return sql; 
    }

    findOne_clt_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed",
                error:err
            }
        }
        return{
            status:true,
            message:"success",
            data:result
        }
    }


/**
 * Update Cloth product
 */
    update_clt(ordered,id){
        let sql = `UPDATE ${this.table} SET ${this.fields.ordered}=${ordered} WHERE ${this.fields.id}=${id}`
        return sql;    

    }

    update_clt_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            success:true,
            message:"vous avez approuvé le produit"
            
        }

    }


/**
 * Delete Cloth product
 */

    delete(id){
        let sql = `DELETE FROM ${this.table} WHERE ${this.fields.id}=${id}`;
        return sql; 
    }

    delete_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed to delete ",
                error:err
            }
        }
        return{
            status:true,
            message:"Product deleted",
            data:result
        }
    }

}




        
