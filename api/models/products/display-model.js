
module.exports= class display {
 
    constructor(){
        this.table="aladin_displays";
        this.response;
        this.fields={id:"disp_id",name:"name_display",price:"price",dim:"dim",image:"img",ordered:"is_ordered",date:"created_at",staff:"staff",comment:"comment"}
    }
/**Tout tester Ok */
/**
 * Get All displays products
 */

    findAll_disp(limit,offset){
      
        let sql= `SELECT * FROM ${this.table} WHERE ${this.fields.ordered}=${1} LIMIT ${limit} OFFSET ${offset}`;
        return sql;
    }


    findAll_disp_callback(err,result,page){

        if(err){
            return{

                status:false,
                message:"failed",
                error:err
            } 
        }
        return {
            status:true,
            page_number:page,
            products_page_number:result.length,
            data:result
        }
        
    }

/**
* POST display product
*/

    create_disp(){
        let sql=`INSERT INTO ${this.table} (name_display,price,dim,is_ordered,created_at,img,staff,comment) values ? `;
        return sql;
   
    }

    create_display_callback(err,result){

        if(err){
            return{
                status:false,
                message:"Mauvais format des données ou champ nom renseignés",
                error:err
                }
        }
        return{
            status:true,
            message:"Les données ont bien été enregistrées!",
            data:result
        }
    }





/**
 * Get single Display product
 */

    findOne_disp(id){
        let sql = `SELECT aladin_displays.disp_id,aladin_displays.price,aladin_displays.dim,aladin_displays.is_ordered,aladin_displays.staff,aladin_displays.user_id,aladin_displays.comment,aladin_displays.created_at,aladin_displays.name_display,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.text,aladin_texts.fontfamily,aladin_texts.y_value,x_value,aladin_texts.fill,aladin_texts.is_back ,aladin_texts.h_y,aladin_texts.w_x,aladin_texts.stroke,aladin_texts.w_stroke FROM aladin_images
        JOIN aladin_displays ON aladin_images.disp=aladin_displays.disp_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        WHERE aladin_displays.disp_id=${id}`;
        return sql; 
    }

    findOne_disp_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed",
                error:err
            }
        }
        return{
            status:true,
            message:"success",
            data:result
        }
    }


/**
 * Update disp product
 */
    update_disp(ordered,id){
        let sql = `UPDATE ${this.table} SET ${this.fields.ordered}=${ordered} WHERE ${this.fields.id}=${id}`
        return sql;    

    }

    update_disp_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            success:true,
            message:"vous avez approuvé le produit"
            
        }

    }


/**
 * Delete Cloth product
 */

    delete_disp(id){
        let sql = `DELETE FROM ${this.table} WHERE ${this.fields.id}=${id}`;
        return sql; 
    }

    delete_disp_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed to delete ",
                error:err
            }
        }
        return{
            status:true,
            message:"Products deleted",
            data:result
        }
    }



}
