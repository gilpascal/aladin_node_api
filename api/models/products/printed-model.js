
module.exports = class printed{
    constructor(){
        this.table ="aladin_printed";
        this.fields ={id:"print_id",name:"name_printed",price:"price",gram:"gram",pellicule:"pellicule",border:"border",volet:"volet",verni:"verni",image:"img",categorie:"cat_id",ordered:"is_ordered",date:"created_at",comment:"comment",staff:"staff"};
    }

/**Tout tester Ok */
/**
* Get ALL printed
*/
    findAll_print(limit,offset){
        let sql=`SELECT * FROM ${this.table} WHERE ${this.fields.ordered}=${1} LIMIT ${limit} OFFSET ${offset}`;
        return sql;
    }

    findAll_print_callback(err,result,page){
        if(err){
            return {
                status:false,
                message:"failed",
                error: err
            }
        }
        return {
            status:true,
            page_number:page,
            products_page_number:result.length,
            data:result
        }
        
    }


/**
* Post Cloth product
*/
    create_printed(){
        let sql=`INSERT INTO ${this.table} (name_printed,price,gram,is_ordered,created_at,img,staff,comment,pellicule,volet) values ? `;
        return sql;
    
    }

    create_printed_callback(err,result){

        if(err){
            return({
                status:false,
                message:"Mauvais format des données ou champ nom renseignés",
                error:err
            })
        }
        return{
            status:true,
            message:"Les données ont bien été enregistrées!",
            data:result
        }

             

    }



/**
* Get single Cloth product
*/

    findOne_print(id){
        let sql = `SELECT aladin_printed.print_id,aladin_printed.price,aladin_printed.dim,aladin_printed.is_ordered,aladin_printed.staff,aladin_printed.user_id,aladin_printed.comment,aladin_printed.created_at,aladin_printed.name_printed,aladin_printed.gram,aladin_printed.border,aladin_printed.pellicule,aladin_printed.verni,aladin_printed.volet,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.text,aladin_texts.fontfamily,aladin_texts.y_value,x_value,aladin_texts.fill,aladin_texts.is_back,aladin_texts.h_y,aladin_texts.w_x,aladin_texts.stroke,aladin_texts.w_stroke FROM aladin_images
        JOIN aladin_printed ON aladin_images.print=aladin_printed.print_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        WHERE aladin_printed.print_id=${id}`;
        return sql; 
    }

    findOne_print_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed",
                error:err
            }
        }
        return{
            status:true,
            message:"success",
            data:result
        }
    }



/**
 * Update printed product
 */
    update_printed(ordered,id){
        let sql = `UPDATE ${this.table} SET ${this.fields.ordered}=${ordered} WHERE ${this.fields.id}=${id}`
        return sql;    

    }

    update_printed_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            success:true,
            message:"vous avez approuvé le produit"
            
        }

    }


/**
* Delete Cloth product
*/

    delete_print(id){
        let sql = `DELETE FROM ${this.table} WHERE ${this.fields.id}=${id}`;
        return sql; 
    }

    delete_print_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed to delete ",
                error:err
            }
        }
        return{
            status:true,
            message:"Product deleted",
            data:result
        }
    }

}