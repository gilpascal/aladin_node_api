
module.exports = class gadgets {
 
    constructor(){
        this.table= "aladin_gadgets";
        this.fields={id:"gadg_id",name:"name_gadget",price:"price",dim:"dim",size:"size",diam:"diam",material:"material",image:"img",ordered:"is_ordered",date:"created_at",comment:"comment",staff:"staff"};
    }

    /**Tout tester Ok */
/**
 * Get All gadget product
 */
    findAll_gadg(limit,offset){
        let sql= `SELECT * FROM ${this.table} WHERE ${this.fields.ordered}=${1} LIMIT ${limit} OFFSET ${offset}`;
        return sql;
    }

    findAll_gadg_callback(err,result,page){
        if(err){
            return{
                status: false,
                message: "failed",
                error:err
                
            }
        }
        return {
            status:true,
            page_number:page,
            products_page_number:result.length,
            data:result
        }
    }


/**
 * Post gadget product
 */
    create_gadget(){
        let sql = `INSERT INTO ${this.table} (name_gadget,price,is_ordered,created_at,img,staff,comment) values ? `
        return sql;
    
    }

    create_gadget_callback(err,result){
    
        if(err){
            return{
                status:false,
                message:"Mauvais format des données ou champ nom renseignés",
                error:err
            }

        }
        return{
            status:true,
            message:"Les données ont bien été enregistrées!",
            data:result

        }

    }

/**
* Post image cloth
*/

    create_img_gadg(req,res){

        let filef=req.files.myfront
        let fileb=req.files.myback
        let filecf=req.files.mycfront
        let filecb = req.files.mycback
        let date= order_date()
        let sql=`INSERT INTO aladin_images(created_at,gadget,front_side,back_side,customimg,custimg) VALUES ?`;
        data=[[date,req.body.disp,"https://api.aladin.ci/gadgets/"+filef.name,"https://api.aladin.ci/gadgets/"+fileb.name,"https://api.aladin.ci/gadgets/"+filecf.name,"https://api.aladin.ci/gadgets/"+filecb.name]];
        
        filef.mv("public/gadgets/"+filef.name,(err,resp)=>{
            if(err) throw err

            fileb.mv("public/gadgets/"+fileb.name,(err,rest)=>{
                if(err) throw err;
            
                filecf.mv("public/gadgets/"+filecf.name,(err,reqs)=>{
                    if(err) throw err;

                    filecb.mv("public/gadgets/"+filecb.name,(error,re)=>{
                        if(error) throw error;
                
                        db.query(sql,[data],(erreur,myresult)=>{
                            if(erreur){
                                res.send({
                                    error:erreur,
                                    status:false,
                                    message:"une erreur s'est produite verifiez le format des données "
                                })
                            }
                            res.send({
                                message:"Les données ont bien étés enregistrés",
                                data:myresult,
                                status:true,
                            })
                        })
        
                    })
                })
        
            })
        })

    }

    create_img_gadg_callback(err,result){
        if(err){
            return{
                status:false,
                message:'Failed',
                error:err
            }
        }
        return{
            status:true,
            message:'Success',
            data: result
        }
    }

/**
* Get One Gadget 
*/

    findOne_gadg(id){
        let sql = `SELECT aladin_gadgets.gadg_id,aladin_gadgets.price,aladin_gadgets.dim,aladin_gadgets.is_ordered,aladin_gadgets.staff,aladin_gadgets.user_id,aladin_gadgets.comment,aladin_gadgets.created_at,aladin_gadgets.name_gadget,aladin_gadgets.size,aladin_gadgets.diam,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.text,aladin_texts.fontfamily,aladin_texts.y_value,x_value,aladin_texts.fill,aladin_texts.is_back,aladin_texts.h_y,aladin_texts.w_x,aladin_texts.stroke,aladin_texts.w_stroke FROM aladin_images
        JOIN aladin_gadgets ON aladin_images.gadget=aladin_gadgets.gadg_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        WHERE aladin_gadgets.gadg_id=${id}`;
        return sql; 
    }

    findOne_gadg_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed",
                error:err
            }
        }
        return{
            status:true,
            message:"success",
            data:result
        }
    }


/**
 * Update gadget product
 */
    update_gadget(ordered,id){
        let sql = `UPDATE ${this.table} SET ${this.fields.ordered}=${ordered} WHERE ${this.fields.id}=${id}`
        return sql;    

    }

    update_gadget_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            success:true,
            message:"vous avez approuvé le produit"
            
        }

    }


/**
* Delete Gadget product
*/

    delete_gadg(id){
        let sql = `DELETE FROM ${this.table} WHERE ${this.fields.id}=${id}`;
        return sql; 
    }

    delete_gadg_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed to delete ",
                error:err
            }
        }
        return{
            status:true,
            message:"Product deleted",
            data:result
        }
    }

}