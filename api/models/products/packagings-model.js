
module.exports = class packagings{

    constructor(){
        this.table = "aladin_packaging";
        this.fields = {id:"pack_id",name:"name_packaging",price:"price",dim:"dim",material:"material",image:"img",ordered:"is_ordered",date:"created_at",comment:"comment",staff:"staff"};
    }

    /**Tout tester Ok */
/**
 * Get All Packagings products
 */
    findAll_pack(limit,offset){
        let sql=`SELECT * FROM ${this.table} WHERE ${this.fields.ordered}=${1} LIMIT ${limit} OFFSET ${offset}`;
        return sql;
    }

    findAll_pack_callback(err,result,page){
        if(err){

            return{
             status: false,
             message: "failed",
             error: err
            }
            
        }
        return {
            status:true,
            page_number:page,
            products_page_number:result.length,
            data:result
        }
    }


/**
* Get single Packaging product
*/

    findOne_pack(id){
        let sql = `SELECT aladin_packaging.pack_id,aladin_packaging.price,aladin_packaging.dim,aladin_packaging.is_ordered,aladin_packaging.staff,aladin_packaging.user_id,aladin_packaging.comment,aladin_packaging.created_at,aladin_packaging.name_packaging,aladin_packaging.material,aladin_images.front_side,aladin_images.back_side,aladin_images.customimg,aladin_images.custimg,aladin_texts.text,aladin_texts.fontfamily,aladin_texts.y_value,x_value,aladin_texts.fill,aladin_texts.is_back,aladin_texts.h_y,aladin_texts.w_x,aladin_texts.stroke,aladin_texts.w_stroke FROM aladin_images
        JOIN aladin_packaging ON aladin_images.pack=aladin_packaging.pack_id 
        JOIN aladin_texts ON aladin_texts.image=aladin_images.id
        WHERE aladin_packaging.pack_id=${id}`;
        return sql; 
    }

    findOne_pack_callback(err,result){
        if(err){
            return{
                status:false,
                message:"failed",
                error:err
            }
        }
        return{
            status:true,
            message:"success",
            data:result
        }
    }


/**
* Post Packaging product
*/

    create_pack(){
        let sql=`INSERT INTO ${this.table} (name_packaging,price,is_ordered,created_at,img,staff,comment,dim,material) values ? `;
        return sql;
    
    }

    create_packagings_callback(err,result){
        
        if(err){
            return{
                status:false,
                message:"Mauvais format des données ou champ nom renseignés",
                error:err
            }
        }
        return{
            status:true,
            message:"Les données ont bien été enregistrées!",
            data:result
        }

    }

/**
 * Update packaging product
 */
    update_packaging(ordered,id){
        let sql = `UPDATE ${this.table} SET ${this.fields.ordered}=${ordered} WHERE ${this.fields.id}=${id}`
        return sql;    

    }

    update_packaging_callback(err,result){
        if(err){
            return{
                status:false,
                error:err
            }
        }
        return{
            success:true,
            message:"vous avez approuvé le produit"
            
        }

    }


/**
* Delete Packaging product
*/

delete_pack(id){
    let sql = `DELETE FROM ${this.table} WHERE ${this.fields.id}=${id}`;
    return sql; 
}

delete_pack_callback(err,result){
    if(err){
        return{
            status:false,
            message:"failed to delete ",
            error:err
        }
    }
    return{
        status:true,
        message:"Product deleted",
        data:result
    }
}

}