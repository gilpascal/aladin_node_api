const express = require('express');
const router = express.Router();
const userscontrol = require('../controllers/users-controller');

/**
* SIGN UP user
*/
router.post("/auth/register",(req,res)=>{
    userscontrol.create_acc_user_callback(req,res);
})

/**
 * LOGIN user
 */
 router.post ("/auth/login",(req,res)=>{
    userscontrol.login_usr_callback(req,res);
})


/**
 * Get All user
 */
    router.get("/",(req,res)=>{
        userscontrol.findAll_usr_callback(req,res);
    })

/**
 * Get one user
 */
    router.get("/:id",(req,res)=>{
        userscontrol.findOne_usr_callback(req,res);
    })

/**
* Delete one user
*/
    router.delete("/:id",(req,res)=>{
        userscontrol.delete_usr_callback(req,res);
    })

/**
 * UPDATE pwd user
 */
    router.put("/reset/password/:id",(req,res)=>{
        userscontrol.reset_pwd_callback(req,res);
    })

/**
* Change Pwd 
*/
    router.put("/change/password/:id",(req,res)=>{
        userscontrol.change_pwd_callback(req,res);
    })

/**
 * UPDATE user details 
 */
    router.put("/details/:id",(req,res)=>{
        userscontrol.update_dtls_callback(req,res);
    })

/**
* Checking Mail
*/
    router.post("/checkmail",(req,res)=>{
        userscontrol.check_mail_callback(req,res);
    })





module.exports= router ;