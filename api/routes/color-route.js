const express = require('express')
const router = express.Router()
const colorcontrol = require('../controllers/color-controller')

/**
 *POST cloth Color
 */
    router.post("/",(req,res)=>{
        colorcontrol.create_color_cloth_callback(req,res);
    })

/**
 *POST cloth Color
 */
 router.post("/",(req,res)=>{
    colorcontrol.create_color_display_callback(req,res);
})

/**
 *POST gadget Color
 */
 router.post("/",(req,res)=>{
    colorcontrol.create_color_gadget_callback(req,res);
})

/**
 *POST packaging Color
 */
 router.post("/",(req,res)=>{
    colorcontrol.create_color_packaging_callback(req,res);
})

/**
 *POST printed Color
 */
 router.post("/",(req,res)=>{
    colorcontrol.create_color_printed_callback(req,res);
})

module.exports = router;