const express = require('express')
const router = express.Router()
const editcolorcontrol = require('../controllers/edit-color-controller')

/**
 * GET Color
 */
    router.get("/",(req,res)=>{
        editcolorcontrol.findAll_edit_colors_calback(req,res)
    })

/**
 * POST Color
 */
    router.post("/",(req,res)=>{
        editcolorcontrol.create_edit_colors_calback(req,res)
    })


module.exports = router;