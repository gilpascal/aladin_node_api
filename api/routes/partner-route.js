var express = require('express');
var router = express.Router();
var partnercontrol = require('../controllers/partner-controller');

/**
* GET details of partner 
*/
    router.get("/details",(req,res)=>{
       partnercontrol.partner_detail_callback(req,res);
    })
/**
* GET details of valid account partner 
*/
    router.get("/",(req,res)=>{
        partnercontrol.partner_valid_acc_callback(req,res);
    })

/**
* PUT activated
*/
    router.put("/:id",(req,res)=>{
        partnercontrol.update_activated_callback(req,res);
    })

module.exports = router;