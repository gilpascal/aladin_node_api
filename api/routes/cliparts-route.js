const express = require('express')
const router = express.Router()
const clipartcontrol = require('../controllers/cliparts-controller')

/**
 * POST Clipart
 */
    router.post("/",(req,res)=>{
        clipartcontrol.create_clips_callback(req,res);
    })

/**
 * GET All cliparts
 */
    router.get("/",(req,res)=>{
        clipartcontrol.findAll_clips_callback(req,res);
    })

/**
 * GET One cliparts
 */
    router.get("/:id",(req,res)=>{
        clipartcontrol.findOne_clips_callback(req,res);
    })

module.exports = router ;