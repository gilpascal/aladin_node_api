const express=require('express');
const router = express.Router();
const shapescontrol=require('../controllers/shapes-controller');

/**
* Get All shpes
*/

    router.get("/",(req,res)=>{
        shapescontrol.findAll_shapes_callback(req,res);
    })

/**
* Get single shape
*/
    router.get("/:id",(req,res)=>{
        shapescontrol.findAll_shapes_callback(req,res);
    })

/**
* POST shape
*/
    router.post("/",(req,res)=>{
        shapescontrol.create_shapes_callback(req,res);
    })
    
module.exports= router;