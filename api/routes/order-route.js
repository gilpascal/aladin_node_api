var express = require('express');
const router = express.Router();
const ordercontrol = require ('../controllers/order-controller');

/**
* POST order
*/
    router.post("/",(req,res)=>{
        ordercontrol.create_ord_callback(req,res);
    })

/**
* GET All pendind orders
*/
    router.get("/pending",(req,res)=>{
        ordercontrol.findAll_pend_callback(req,res);
    })

/**
* GET All order delivered
*/
    router.get("/Ok",(req,res)=>{
        ordercontrol.findAll_deliver_callback(req,res);
    })

/**
* GET All news order 
*/
    router.get("/rejected",(req,res)=>{
        ordercontrol.findAll_reject_callback(req,res);
    })

/**
* GET All news order 
*/
    router.get("/news",(req,res)=>{
        ordercontrol.findAll_new_callback(req,res);
    })

/**
* GET All  orders customer
*/
    router.get("/customers/:id",(req,res)=>{
        ordercontrol.findAll_order_cust_callback(req,res);
    })

/**
* PUT  order Ok
*/
    router.put("/Ok/:id",(req,res)=>{
        ordercontrol.update_stts_Ok_callback(req,res);
    })

/**
* PUT  order pending
*/
    router.put("/pending/:id",(req,res)=>{
        ordercontrol.update_stts_pending_callback(req,res);
    })

/**
* PUT  order rejected
*/
    router.put("/rejected/:id",(req,res)=>{
        ordercontrol.update_stts_rejected_callback(req,res);
    })



module.exports= router ;
    
