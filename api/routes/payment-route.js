require('dotenv').config()
var express = require('express');
var router = express.Router();
const request = require("request");
const uuid  = require('uuid');

router.get("/token",(req,resp)=>{
  
var encodedData = Buffer.from(process.env.API_USER+ ':' +process.env.API_KEY).toString('base64');
var authorizationHeaderString = 'Basic ' + encodedData;
 const url= process.env.BaseUrl+"token/"

  const options = {
    url: url,
    headers: {
      'Ocp-Apim-Subscription-Key':process.env.PRIMARY_KEY,
      'Authorization':authorizationHeaderString
     },
     method:"POST"
  };
  
  request(options,(err,res,body)=>{
    if (!err && res.statusCode == 200) {
      const token = JSON.parse(body);
      resp.send({
      status:true,
      code:200,
      token:token
      }
      );
    }else{
      console.log(err);
      resp.send(
        {
          status:false,
          code:404,
          error:err
        }
      )
    }
  });
 
})

router.get('/balance',(req,res)=>{
  const url= process.env.BaseUrl+"v1_0/account/balance"
console.log(req.headers.authorization);
  const options = {
    url: url,
    headers: {
      'Ocp-Apim-Subscription-Key':process.env.PRIMARY_KEY,
      'authorization':req.headers.authorization,
      'X-Target-Environment':process.env.X_Target_Environment
    },
       method:"GET"
  
}; 
  request(options,(err,resp,body)=>{
    if (resp.statusCode == 200) {
      const amount = JSON.parse(body);
      res.send({
      status:true,
      code:200,
      amount:amount
      }
      );
    }else{
      console.log(err);
      res.send(
        {
          status:false,
          code:resp.statusCode,
          error:err
        }
      )
    }
  });

})

router.post("/",(req,res)=>{
  const ref=uuid.v4();
  const body ={
    "amount": req.body.amount,
    "currency":"XOF",
    "externalId":ref,
    "payer": {
      "partyIdType": "MSISDN",
      "partyId":req.body.phone
    },
    "payerMessage": "Achat de produit sur aladin.ci",
    "payeeNote": "vente de produit"
  }
  const options = {
    url: process.env.BaseUrl+'v1_0/requesttopay',
    headers: {
      'authorization':req.headers.authorization,
      'X-Callback-Url':process.env.CALLBACK_HOST_URL,
      'X-Reference-Id':ref,
      'X-Target-Environment':process.env.X_Target_Environment,
      'Content-Type': 'application/json',
      'Ocp-Apim-Subscription-Key':process.env.PRIMARY_KEY 
    },
    body:JSON.stringify(body),
    method:"POST"
  };
   request(options,(err,response)=>{
     if(response.statusCode==202){
       console.log(response);
       res.send(
         {
           status:true,
           code:response.statusCode,
           ref:ref
          }
       )
      
     }else{
       if(response.statusCode==400){
         res.send(
           {
             stutus:false,
             code:response.statusCode,
             error:err,
           }
         )
       }else{
        res.send(
          {
            stutus:false,
            code:response.statusCode,
            error:err,
          }
        )
       }
     }
   })
})

router.get('/status/:ref',(req,res)=>{
  const options = {
    url: process.env.BaseUrl+'v1_0/requesttopay/'+req.params.ref,
    headers: {
      'authorization':req.headers.authorization,
      'X-Target-Environment':process.env.X_Target_Environment,
      'Content-Type':'application/json',
      'Ocp-Apim-Subscription-Key':process.env.PRIMARY_KEY 
    },
    method:"GET"
  };
  request(options,(error,resp,body)=>{
    if(resp.statusCode==200){
      res.send({
        status:true,
        code:resp.statusCode,
        data:JSON.parse(body)
      })
    }else{
      res.send({
        status:false,
        code:resp.statusCode,
        error:error,
        body:JSON.parse(resp.body)
      })
    }
  })

})



module.exports = router
