const express = require('express');
const router = express.Router();
const staffcontrol = require('../controllers/staff-controller');

/**
* SIGN UP staff 
*/
    router.post("/register",(req,res)=>{
        staffcontrol.create_acc_staff_callback(req,res);
    })

/**
* LOGIN staff 
*/
    router.post("/login",(req,res)=>{
        staffcontrol.login_staf_callback(req,res);
    })

/**
* Find All staff 
*/
    router.get("/",(req,res)=>{
        staffcontrol.findAll_staf_callback(req,res);
    })

/**
* Find designer from staff
*/
    router.get("/designer",(req,res)=>{
        staffcontrol.findAll_staf_designer_callback(req,res);
    })

/**
* Find designer from staff
*/
    router.get("/conseillé_client",(req,res)=>{
        staffcontrol.findAll_staf_cc_callback(req,res);
    })

/**
* change password staff
*/
    router.put("/change/password/:id",(req,res)=>{
        staffcontrol.change_password_callback(req,res);
    })

/**
* reset password staff
*/
    router.put("/reset/password/:id",(req,res)=>{
        staffcontrol.reset_pwd_staff_callback(req,res);
    })

/**
* Find admin from staff
*/
    router.get("/admin",(req,res)=>{
        staffcontrol.findAll_staf_admin_callback(req,res);
    })

/**
* Delete one user
*/
    router.delete("/:id",(req,res)=>{
        staffcontrol.delete_staf_callback(req,res);
    })

module.exports = router ;