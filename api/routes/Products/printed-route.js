const express = require('express');
const router = express.Router();
const printedcontrol = require('../../controllers/products/printed-controller');

/*GET All printed*/

router.get("/",(req,res)=>{
    printedcontrol.findAll_print_callback(req,res);
})

/*GET one printed*/
router.get("/:id",(req,res)=>{
    printedcontrol.findOne_print_callback(req,res);
})

/*POST printed*/
router.post("/",(req,res)=>{
    printedcontrol.create_print_callback(req,res);
})

/*update cloth*/
    router.put("/:id",(req,res)=>{
        printedcontrol.update_print_callback(req,res);
    })


/*DELETE cloth*/
router.delete("/:id",(req,res)=>{
    printedcontrol.delete_print_callback(req,res);
})

module.exports= router;