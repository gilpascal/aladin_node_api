const express = require ('express');
const router = express.Router();
const displayimgcontrol = require ('../../controllers/products/display-img-controller');

/**
* POST image display
*/
    router.post("/images",(req,res)=>{
        displayimgcontrol.create_img_disp_callback(req,res);
    })

module.exports = router;