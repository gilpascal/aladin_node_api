const express = require('express');
const router= express.Router();
const packagingscontrol = require('../../controllers/products/packagings-controller');

/*get All packaging products */
router.get("/",(req,res)=>{
    packagingscontrol.findAll_pack_callback(req,res);

})

/*get single packaging product*/
router.get("/:id",(req,res)=>{
    packagingscontrol.findOne_pack_callback(req,res);
})

/*post packaging product*/
router.post("/",(req,res)=>{
    packagingscontrol.create_pack_callback(req,res);
})

/*update cloth*/
    router.put("/:id",(req,res)=>{
        packagingscontrol.update_gad_callback(req,res);
    })

/*delete one packaging product*/
router.delete("/:id",(req,res)=>{
    packagingscontrol.delete_pack_callback(req,res);
})

module.exports = router;

