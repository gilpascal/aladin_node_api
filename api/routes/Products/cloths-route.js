const express = require ('express');
const router = express.Router();
const clothcontrol = require ('../../controllers/products/cloths-controller');

/*get all cloths*/
router.get("/",(req,res)=>{
    clothcontrol.findAll_cloth_callback(req,res);
})

router.get("/Home",(req,res)=>{
    clothcontrol.findAll_clot_callback(req,res);
})

router.get("/Items",(req,res)=>{
    clothcontrol.findAll_clo_callback(req,res);
})

/*get one cloth*/
router.get("/:id",(req,res)=>{
    clothcontrol.findOne_cloth_callback(req,res);
})

/*post cloth*/
router.post("/",(req,res)=>{
    clothcontrol.create_cloth_callback(req,res);
})

/*update cloth*/

    router.put("/:id",(req,res)=>{
        clothcontrol.update_cloth_callback(req,res);
    })


/*delete cloth*/
router.delete("/:id",(req,res)=>{
    clothcontrol.delete_cloth_callback(req,res);
})

module.exports = router;

