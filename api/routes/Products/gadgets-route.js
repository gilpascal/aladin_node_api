const express = require('express');
const router = express.Router();
const gadgetscontrol = require ('../../controllers/products/gadgets-controller');

/*get All gadgets products */

router.get("/",(req,res) =>{
    gadgetscontrol.findAll_gadg_callback(req,res);

})

/*get single gagdet product */
router.get("/:id",(req,res)=>{
    gadgetscontrol.findOne_gadg_callback(req,res);
})

/*post gadget product */
router.post("/",(req,res)=>{
    gadgetscontrol.create_gadg_callback(req,res);
})

/*update cloth*/
    router.put("/:id",(req,res)=>{
        gadgetscontrol.update_gad_callback(req,res);
    })

/*delete one gadget product */
router.delete("/:id",(req,res)=>{
    gadgetscontrol.delete_gadg_callback(req,res);
})

module.exports = router;