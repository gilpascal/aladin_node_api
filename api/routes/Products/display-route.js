const express = require('express');
const router = express.Router();
const displaycontrol = require('../../controllers/products/display-controller');

/*get all display*/

router.get("/",(req,res) =>{
    displaycontrol.findAll_disp_callback(req,res);
})
/*get one display*/
router.get("/:id",(req,res)=>{
    displaycontrol.findOne_disp_callback(req,res);
})

/*post display*/
router.post("/",(req,res)=>{
    displaycontrol.create_disp_callback(req,res);
})

/*update display*/
    router.put("/:id",(req,res)=>{
        displaycontrol.update_disp_callback(req,res);
    })

/*delete display*/
router.delete("/:id",(req,res)=>{
    displaycontrol.delete_disp_callback(req,res);
})
module.exports = router;
