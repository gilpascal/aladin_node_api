const express = require ('express');
const router = express.Router();
const packagingsimgcontrol = require ('../../controllers/products/packagings-img-controller');

/**
* POST image display
*/
    router.post("/images",(req,res)=>{
        packagingsimgcontrol.create_img_pack_callback(req,res);
    })


module.exports = router;