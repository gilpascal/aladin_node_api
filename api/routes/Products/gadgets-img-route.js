const express = require ('express');
const router = express.Router();
const gadgetsimgcontrol = require ('../../controllers/products/gadgets-img-controller');

/**
* POST image display
*/
    router.post("/images",(req,res)=>{
        gadgetsimgcontrol.create_img_gadg_callback(req,res);
    })

module.exports = router;