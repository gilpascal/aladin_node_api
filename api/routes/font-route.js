const express = require('express')
const router = express.Router()
const fontcontrol = require('../controllers/font-controller')

/**
 * POST font
 */
    router.post("/",(req,res)=>{
        fontcontrol.create_fnt_callback(req,res)
    })

/**
 * GET font
 */
    router.get("/",(req,res)=>{
        fontcontrol.findAll_fnt_callback(req,res)
    })


/**
 * GET font by id
 */
    router.get("/:id",(req,res)=>{
        fontcontrol.findOne_fnt_callback(req,res)
    })


module.exports = router;