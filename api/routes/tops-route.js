var express = require('express');
var router = express.Router();
var topscontrol = require('../controllers/tops-controller');

/**
 * Find All tops
 */
    router.get("/",(req,res)=>{
        res.send(topscontrol.findAll_tps_callback(req,res));
    })

/**
 * Get single tops
 */
    router.get("/:id",(req,res)=>{
        res.send(topscontrol.findOne_tps_callback(req,res));
    })

/**
* Post tops
*/
    router.post("/",(req,res)=>{
        res.send(topscontrol.create_tps_callback(req,res));
    })

module.exports = router;