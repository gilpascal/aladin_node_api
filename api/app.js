var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fileupload = require("express-fileupload")
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
//import users route
const userRouter = require('./routes/users-route');
//import font route
const fontRouter = require('./routes/font-route');
//import pay route
const payRouter = require('./routes/payment-route');
//import users route
const Prod_orderRouter = require('./routes/prod_order-route');
//import cliparts route
const clipartsRouter = require('./routes/cliparts-route');
//import color route
const edit_colorRouter = require('./routes/edit-color-route');
//import color route
const colorRouter = require('./routes/color-route');
//import text route
const textRouter = require('./routes/text-route');
//import partner route
const partnerRouter = require('./routes/partner-route');
//import staff route
const staffRouter = require('./routes/staff-route');
//import cloths route
const clothsRouter = require('./routes/Products/cloths-route');
//import cloths image route
const clothsimgRouter = require('./routes/Products/cloths-img-route');

//import displays route
const displaysRouter = require('./routes/Products/display-route');
//import displays image route
const displaysimgRouter = require('./routes/Products/display-img-route');

//import gadgets route
const gadgetsRouter = require('./routes/Products/gadgets-route');
//import gadgets image route
const gadgetsimgRouter = require('./routes/Products/gadgets-img-route');

//import packagings route
const packagingsRouter = require('./routes/Products/packagings-route');
//import packagings image route
const packagingsimgRouter = require('./routes/Products/packagings-img-route');

//import printed route
const printedRouter = require('./routes/Products/printed-route');
//import printed image route
const printedimgRouter = require('./routes/Products/printed-img-route');

//import order route
const ordersRouter = require('./routes/order-route');
//import tops route
const topsRouter = require('./routes/tops-route');
//import shapes route
const shapesRouter = require('./routes/shapes-route');




var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileupload());

app.use('/', indexRouter);
app.use('/users', usersRouter);
//create users route
app.use('/api/v1/users',userRouter);
//create payment route
app.use('/api/v1/pay',payRouter);
//create prod_order route
app.use('/api/v1/product_order',Prod_orderRouter);
//create users route
app.use('/api/v1/cliparts',clipartsRouter);
//create color route
app.use('/api/v1/colors',colorRouter);
//create color route
app.use('/api/v1/edit_colors',edit_colorRouter);
//create text route
app.use('/api/v1/texts',textRouter);
//create font route
app.use('/api/v1/font',fontRouter);

//create users route
app.use('/api/v1/partner',partnerRouter);

//create staff route
app.use('/api/v1/staff',staffRouter);

//create cloths route
app.use('/api/v1/cloths',clothsRouter);
//create cloths image route
app.use('/api/v1/cloths',clothsimgRouter);

//create display route
app.use('/api/v1/displays',displaysRouter);
//create display image route
app.use('/api/v1/displays',displaysimgRouter);

//create gadgets route
app.use('/api/v1/gadgets',gadgetsRouter);
//create gadgets image route
app.use('/api/v1/gadgets',gadgetsimgRouter);

//create gadgets route
app.use('/api/v1/packagings',packagingsRouter);
//create gadgets image route
app.use('/api/v1/packagings',packagingsimgRouter);


//create printed route
app.use('/api/v1/printed',printedRouter);
//create printed image route
app.use('/api/v1/printed',printedimgRouter);

//create shapes route
app.use('/api/v1/orders',ordersRouter);

//create shapes route
app.use('/api/v1/tops',topsRouter);

//create shapes route
app.use('/api/v1/shapes',shapesRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
